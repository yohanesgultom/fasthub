<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PageController@welcome')->name('welcome');
Route::get('/terms-conditions', 'PageController@termsConditions')->name('terms-conditions');
Route::post('/contactUs', 'PageController@contactUs')->name('contactUs');

// User
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');
Route::get('verifyAccount/{token}', 'Auth\RegisterController@verifyAccount')->name('user.verifyAccount');
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');
Route::middleware(['auth'])->group(function() {
    Route::get('home', 'User\HomeController@index')->name('user.home');
    Route::get('profile', 'User\HomeController@showUpdateForm')->name('user.update');
    Route::get('chat/{account_id}', 'User\HomeController@showChatForm')->name('user.chat');
    Route::get('showCompany/{id}', 'User\HomeController@showCompanyForm')->name('user.showCompany');
    Route::get('showChangePasswordForm', 'User\HomeController@showChangePasswordForm')->name('user.showChangePassword');
    Route::get('showMessagesForm', 'User\HomeController@showMessagesForm')->name('user.showMessagesForm');
    Route::post('chat/{account_id}', 'User\UserController@reply')->name('user.reply');
    Route::post('profile', 'User\UserController@update');
    Route::post('review/{account_id}', 'User\UserController@review')->name('user.review');
    Route::post('changePassword', 'User\UserController@changePassword')->name('user.changePassword');
});

// Company
Route::prefix('company')->group(function() {    
    Route::get('login', 'Company\Auth\LoginController@showLoginForm')->name('company.login');
    Route::post('login', 'Company\Auth\LoginController@login');
    Route::post('logout', 'Company\Auth\LoginController@logout')->name('company.logout');    
    Route::get('register', 'Company\Auth\RegisterController@showRegistrationForm')->name('company.register');
    Route::post('register', 'Company\Auth\RegisterController@register');
    Route::get('verifyAccount/{token}', 'Company\Auth\RegisterController@verifyAccount')->name('company.verifyAccount');
    Route::get('password/reset', 'Company\Auth\ForgotPasswordController@showLinkRequestForm')->name('company.password.request');
    Route::post('password/email', 'Company\Auth\ForgotPasswordController@sendResetLinkEmail')->name('company.password.email');
    Route::get('password/reset/{token}', 'Company\Auth\ResetPasswordController@showResetForm')->name('company.password.reset');
    Route::post('password/reset', 'Company\Auth\ResetPasswordController@reset');    
});

Route::prefix('company')->middleware(['auth:company'])->group(function() {    
    Route::get('', 'Company\HomeController@index')->name('company.home');
    Route::get('profile', 'Company\HomeController@showUpdateForm')->name('company.update');
    Route::get('chat/{account_id}', 'Company\HomeController@showChatForm')->name('company.chat');
    Route::get('showUser/{id}', 'Company\HomeController@showUserForm')->name('company.showUser');
    Route::get('showChangePasswordForm', 'Company\HomeController@showChangePasswordForm')->name('company.showChangePassword');
    Route::get('showMessagesForm', 'Company\HomeController@showMessagesForm')->name('company.showMessagesForm');
    Route::post('chat/{account_id}', 'Company\CompanyController@reply')->name('company.reply');
    Route::post('profile', 'Company\CompanyController@update');
    Route::post('review/{account_id}', 'Company\CompanyController@review')->name('company.review');
    Route::post('changePassword', 'Company\CompanyController@changePassword')->name('company.changePassword');
});
