#!/bin/bash

(php artisan serve -q --port=8888 --env=dusk.local) &
(php artisan dusk) &
# (php artisan dusk tests/Browser/CompanyTest.php) &
wait -n
kill 0