<?php

use Illuminate\Database\Seeder;

class CompaniesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $companies = factory(App\Company::class, 1)->create([
            'email' => 'crevazze@gmail.com',
            'password' => bcrypt('crevazze123'),
        ]);
        $companies = $companies->concat(factory(App\Company::class, 10)->create());
    }
}
