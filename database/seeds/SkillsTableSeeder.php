<?php

use Illuminate\Database\Seeder;

class SkillsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('skills')->insert([
            ['name' => 'sales'],
            ['name' => 'marketing'],
            ['name' => 'programming'],
            ['name' => 'business development'],
            ['name' => 'hrd'],
            ['name' => 'supply chain'],
            ['name' => 'design'],
            ['name' => 'legal'],
            ['name' => 'engineering'],
            ['name' => 'e-commerce'],
            ['name' => 'administration'],
            ['name' => 'finance'],
            ['name' => 'accountancy'],
            ['name' => 'research'],
        ]);
    }
}
