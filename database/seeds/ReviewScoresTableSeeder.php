<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Carbon\Carbon;

class ReviewScoresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $reviews = collect([]);
        $users = App\User::all();
        $companies = App\Company::all();
        $review_body = [
            '',
            'Not recommended',
            'There is a lot of room for improvement',
            'Not bad',
            'Good job',
            'Perfect!',
        ];
        $reviewers = App\Company::where('email', '!=', 'crevazze@gmail.com')->get();
        foreach ($users as $u) {
            foreach ($reviewers as $c) {
                $score = $faker->numberBetween(1, 5);
                $body = $review_body[$score];
                $reviews->push([
                    'score' => $score,
                    'body' => $body,
                    'reviewee_id' => $u->account_id,
                    'reviewer_id' => $c->account_id,
                    'public' => true,
                    'created_at' => new Carbon,
                ]);    
            }
        }

        // review from user
        $reviewers = $users->where('email', '!=', 'yohanes.gultom@gmail.com')->all();
        foreach ($companies as $c) {
            foreach ($reviewers as $u) {
                $score = $faker->numberBetween(1, 5);
                $body = $review_body[$score];
                $reviews->push([
                    'score' => $score,
                    'body' => $body,
                    'reviewee_id' => $c->account_id,
                    'reviewer_id' => $u->account_id,
                    'public' => true,
                    'created_at' => new Carbon,
                ]);    
            }
        }

        DB::table('review_scores')->insert($reviews->toArray());
    }
}
