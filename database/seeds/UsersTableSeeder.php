<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = factory(App\User::class, 1)->create([
            'email' => 'yohanes.gultom@gmail.com',
            'password' => bcrypt('yohanes123'),
        ]);
        $users = $users->concat(factory(App\User::class, 10)->create());
        
        $users->each(function ($u) {            
            // create skills
            $skills = DB::table('skills')->inRandomOrder()->take(3)->pluck('id');
            $u->skills()->attach($skills);
            $u->save();            
        });
    }
}
