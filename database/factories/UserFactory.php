<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {    
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'verified' => true,
        'password' => bcrypt('secret'),
        'title' => $faker->jobTitle,
        'summary' => $faker->paragraph(),
        'experience' => array_reduce($faker->paragraphs(), function($reduced, $val) { return $reduced.'<p>'.$val.'</p>'; }),
        'education' => '<ol>'.array_reduce($faker->sentences(), function($reduced, $val) { return $reduced.'<li>'.$val.'</li>'; }).'</ol>',
    ];
});
