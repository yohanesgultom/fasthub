<?php

use Faker\Generator as Faker;

$factory->define(App\Company::class, function (Faker $faker) {
    $name = $faker->name;
    $company_name = $faker->company;
    $domain = str_slug($company_name).'.com';
    return [
        'name' => $name,
        'company_name' => $company_name,
        'position' => $faker->jobTitle,
        'domain' => $faker->randomElement(['Pharmacy', 'IT', 'Oil & Gas', 'Property', 'Housing', 'Consultancy', 'Mining', 'Education', 'Telecommunication', 'Energy', 'Venture Capital', 'Finance', 'Health']),
        'email' => 'hrd@'.$domain,
        'verified' => true,
        'url' => 'https://www.'.$domain.'/about-us',
        'summary' => $faker->paragraph(),
        'problems' => $faker->paragraph(),
        'password' => bcrypt('secret'),
    ];
});
