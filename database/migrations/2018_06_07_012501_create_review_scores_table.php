<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewScoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('review_scores', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('score');
            $table->text('body');
            $table->boolean('public')->default(false);
            $table->unsignedInteger('reviewee_id');
            $table->unsignedInteger('reviewer_id');
            $table->timestamps();
            $table->foreign('reviewee_id')->references('id')->on('accounts');            
            $table->foreign('reviewer_id')->references('id')->on('accounts');
            $table->unique(['reviewee_id', 'reviewer_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('review_scores');
    }
}
