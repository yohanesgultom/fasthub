<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use App\Company;
use App\User;

class CompanyTest extends DuskTestCase
{
    public function testProfileUpdate()
    {
        $company = factory(Company::class, 1)->create([
            'name' => 'Test Employee',
            'email' => 'admin@test.com',
            'password' => bcrypt('test123'),
        ])->first();

        $data = [
            'name' => 'Test Employee',
            'position' => 'CEO/CFO/CTO/Cleaning Service',
            'company_name' => 'The Best Company Ever',
            'domain' => 'Marketing',
            'url' => 'https://www.the-best-company-ever.tk',
            'summary' => 'This is a super long summary',
            'problems' => 'Nobody want to use our service',
        ];

        $this->browse(function ($browser) use ($company, $data) {
            $browser->loginAs($company, 'company')
                ->visit(route('company.home', [], false))
                ->assertSeeLink($company->name)
                ->clickLink(__('Edit profile'))
                ->assertSee(__('Employee Profile'))
                ->type('input[name=name]', $data['name'])
                ->type('input[name=position]', $data['position'])
                ->type('input[name=company_name]', $data['company_name'])
                ->type('input[name=domain]', $data['domain'])
                ->type('input[name=url]', $data['url']);
            $browser->script('$("textarea[name=summary]").summernote("reset")');
            $browser->script('$("textarea[name=problems]").summernote("reset")');    
            $browser->script('$("textarea[name=summary]").summernote("insertText", "'.$data['summary'].'")');
            $browser->script('$("textarea[name=problems]").summernote("insertText", "'.$data['problems'].'")');
            $browser->attach('input[name=logo]', base_path('public'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'avatar.png'))
                ->pause(500)
                ->click('form > button[type=submit]')
                ->assertSee(__('Profile updated'))
                ->clickLink($company->name)
                ->clickLink(__('Logout'))                
                ->assertPathIs('/');
        });

        // check database
        $updated_company = Company::find($company->id);
        $this->assertEquals($data['name'], $updated_company->name);
        $this->assertEquals($data['position'], $updated_company->position);
        $this->assertEquals($data['company_name'], $updated_company->company_name);
        $this->assertEquals($data['url'], $updated_company->url);
        $this->assertEquals($data['summary'], strip_tags($updated_company->summary));
        $this->assertEquals($data['problems'], strip_tags($updated_company->problems));
    }

    public function testFacilitatorList()
    {
        $users = factory(User::class, 10)->create();
        $company = factory(Company::class, 1)->create(['name' => 'Test LTD'])->first();
        $this->browse(function ($browser) use ($company) {
            $browser->loginAs($company, 'company')
                ->visit(route('company.home', [], false))
                ->assertSeeLink($company->name)
                ->assertSee(__('Facilitators'))
                ->assertVisible('.pagination')
                ->assertVisible('.card-body > a.btn-details')
                ->click('.card-body > a.btn-details')
                ->assertSee(__('Facilitator Profile'))
                ->clickLink('<< '.__('Back'))
                ->assertPathIs(route('company.home', [], false))
                ->assertVisible('.card-body > a.btn-chat')
                ->click('.card-body > a.btn-chat')
                ->assertSee(__('Send Message'))
                ->clickLink('<< '.__('Back'))
                ->assertPathIs(route('company.home', [], false))
                ->clickLink(__('Logout'))                
                ->assertPathIs('/');
        });
    }

    public function testChangePassword()
    {
        $company = factory(Company::class, 1)->create([
            'name' => 'Test Company',
            'email' => 'admin@test.com',
            'password' => bcrypt('test123'),
        ])->first();

        $this->browse(function ($browser) use ($company) {
            $browser->loginAs($company, 'company')
                ->visit(route('company.home', [], false))
                ->assertSeeLink($company->name)
                ->clickLink(__('Change Password'))
                ->pause(500)
                ->assertSee(__('Change Password'))                                
                ->type('input[name=password]', 'test123')
                ->type('input[name=password_new]', 'test456')
                ->type('input[name=password_new_confirmation]', 'test456')
                ->click('form > button[type=submit]')
                ->assertSee(__('Password changed'))
                ->assertPathIs(route('company.home', [], false))
                ->clickLink($company->name)
                ->clickLink(__('Logout'))                
                ->assertPathIs('/');
        });

    }
}
