<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use App\User;
use App\Company;

class UserTest extends DuskTestCase
{
    public function testCompanyList()
    {
        $companies = factory(Company::class, 10)->create();
        $user = factory(User::class, 1)->create()->first();
        $this->browse(function ($browser) use ($user) {
            $browser->loginAs($user)
                ->visit(route('user.home', [], false))
                ->assertSeeLink($user->name)
                ->assertSee(__('Employees'))
                ->assertVisible('.pagination')
                ->assertVisible('.card-body > a.btn-details')
                ->click('.card-body > a.btn-details')
                ->assertSee(__('Employee Profile'))
                ->clickLink('<< '.__('Back'))
                ->assertPathIs(route('user.home', [], false))
                ->assertVisible('.card-body > a.btn-chat')
                ->click('.card-body > a.btn-chat')
                ->assertSee(__('Send Message'))
                ->clickLink('<< '.__('Back'))
                ->assertPathIs(route('user.home', [], false))
                ->clickLink(__('Logout'))                
                ->assertPathIs('/');
        });
    }

    public function testProfileUpdate()
    {
        $user = factory(User::class, 1)->create([
            'name' => 'Test Facilitator',
            'email' => 'test@email.com',
            'password' => bcrypt('test123'),
        ])->first();

        $data = [
            'title' => 'A test subject',
            'summary' => 'This is my super long summary',
            'skills' => ['testing', 'gaming'],
        ];

        $this->browse(function ($browser) use ($user, $data) {
            $browser->loginAs($user)
                ->visit('/home')
                ->assertSeeLink($user->name)
                ->clickLink(__('Edit profile'))
                ->assertSee(__('Facilitator Profile'))
                ->type('input[name=title]', $data['title'])
                ->type('.form-group-summary .note-editable', $data['summary'])
                ->attach('input[name=photo]', base_path('public'.DIRECTORY_SEPARATOR.'img'.DIRECTORY_SEPARATOR.'avatar.png'))
                ->append('.bootstrap-tagsinput input[type=text]', $data['skills'][0])
                ->keys('.bootstrap-tagsinput input[type=text]', '{enter}')
                ->append('.bootstrap-tagsinput input[type=text]', $data['skills'][1])
                ->keys('.bootstrap-tagsinput input[type=text]', '{enter}')
                ->pause(500)
                ->click('form > button[type=submit]')
                ->assertSee(__('Profile updated'))
                ->clickLink($user->name)
                ->clickLink(__('Logout'))                
                ->assertPathIs('/');
        });

        // check database
        $updated_user = User::with('skills')->find($user->id);
        $this->assertEquals($data['title'], $updated_user->title);
        $this->assertEquals($data['summary'], $updated_user->summary);
        $this->assertEquals($data['skills'], $updated_user->skills->pluck('name')->all());
    }

    public function testChangePassword()
    {
        $user = factory(User::class, 1)->create([
            'name' => 'Test Facilitator',
            'email' => 'test@email.com',
            'password' => bcrypt('test123'),
        ])->first();

        $this->browse(function ($browser) use ($user) {
            $browser->loginAs($user)
                ->visit(route('user.home', [], false))
                ->assertSeeLink($user->name)
                ->clickLink(__('Change Password'))
                ->assertSee(__('Change Password'))
                ->type('input[name=password]', 'test123')
                ->type('input[name=password_new]', 'test456')
                ->type('input[name=password_new_confirmation]', 'test456')
                ->click('form > button[type=submit]')
                ->assertSee(__('Password changed'))
                ->assertPathIs(route('user.home', [], false))
                ->clickLink($user->name)
                ->clickLink(__('Logout'))                
                ->assertPathIs('/');
        });

    }    

}
