<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use App\User;
use App\Company;

class AuthTest extends DuskTestCase
{
    public function testRegister()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                ->assertSee(config('app.name'))
                ->visit('/register')
                ->assertSee( __('Login'))
                ->clickLink( __('Register'))
                ->assertSee(__('Facilitator Registration'))
                ->assertVisible('input[name=title]');
            
            $browser->visit('/')
                ->visit('/company/register')
                ->assertSee( __('Login'))
                ->clickLink( __('Register'))
                ->assertSee(__('Employee Registration'))
                ->assertVisible('input[name=name]')
                ->assertVisible('input[name=company_name]');
        });
    }

    public function testLogin()
    {
        $this->browse(function ($browser) {
            $user = factory(User::class, 1)->create([
                'name' => 'Test Facilitator',
                'email' => 'test@email.com',
                'password' => bcrypt('test123'),
            ])->first();

            $browser->loginAs($user)
                ->visit('/home')
                ->assertSeeLink($user->name)
                ->clickLink($user->name)
                ->clickLink(__('Logout'))                
                ->assertPathIs('/');

            $company = factory(Company::class, 1)->create([
                'name' => 'Test Employee',
                'email' => 'testemployee@email.com',
                'password' => bcrypt('test123'),
            ])->first();

            $browser->loginAs($company, 'company')
                ->visit('/company')
                ->assertSeeLink($company->name)
                ->clickLink($company->name)
                ->clickLink(__('Logout'))                
                ->assertPathIs('/');
        });
    }
}
