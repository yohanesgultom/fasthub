<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;
    use RefreshDatabase;
    use WithFaker;

    protected function setUp()
    {
        parent::setUp();
        \Artisan::call('db:seed');            
        \Storage::fake('test');
    }

    public function assertResponse($action, $url, $status, $user = null, $guard = 'web')
    {
        if ($user == null) {
            $response = $this->json($action, $url);
        } else {
            $response = $this->actingAs($user, $guard)->json($action, $url);
        }

        $this->assertStatus($status, $response);

        // return for further testing
        return $response;
    }

    public function assertStatus($status, $response)
    {
        if ($response->status() != $status) {
            $json = $response->json();            
            if (array_key_exists('message', $json)) {
                if (array_key_exists('file', $json)) print($json['file'].':');
                if (array_key_exists('line', $json)) print($json['line'].':');
                print($json['message']);
            } else {
                print_r($json);
            }            
        }
        $response->assertStatus($status);
    }

}
