<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Jobs\UnreadMessageJob;
use App\Mail\UnreadMessage;
use App\Service\MessageService;
use App\Company;
use App\User;
use Carbon\Carbon;
use Mail;

class UnreadMessageJobTest extends TestCase
{
    public function testHandle()
    {
        Mail::fake();
        $company = Company::inRandomOrder()->first();
        $user = User::inRandomOrder()->first();              
        $thread = MessageService::createThread($this->faker->sentence(), $user->account_id, $company->account_id);
        $sentences = $this->faker->sentences(2);
        MessageService::addMessage($thread, $user->account_id, $sentences[0]);
        MessageService::addMessage($thread, $user->account_id, $sentences[1]);
        $this->assertNull($company->account->last_email);

        // run the job
        $job = new UnreadMessageJob();
        $job->handle();
        Mail::assertQueued(UnreadMessage::class, function ($mail) use ($company) {
            return $mail->hasTo($company->email) 
                && ($mail->count == 1);
        }, 1);
        $company->refresh();
        $this->assertNotNull($company->account->last_email);

        // should not send any email
        $job->handle();
        Mail::assertQueued(UnreadMessage::class, 1);

        // should send again when 
        // last_email is at least 3 days ago
        $company->account->last_email = Carbon::now()->subDays(3);
        $company->account->save();
        $job->handle();
        Mail::assertQueued(UnreadMessage::class, 2);
    }
}
