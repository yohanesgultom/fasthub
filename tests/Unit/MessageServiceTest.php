<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Service\MessageService;
use App\Account;
use App\Company;
use App\User;
use Carbon\Carbon;

class MessageServiceTest extends TestCase
{
    public function testCreateThread()
    {
        $user = User::inRandomOrder()->first();
        $company = Company::inRandomOrder()->first();
        $subject = $this->faker->sentence();
        $thread = MessageService::createThread($subject, $user->account_id, $company->account_id);
        $this->assertEquals($subject, $thread->subject);
    }

    public function testAddMessage()
    {
        $user = User::inRandomOrder()->first();
        $company = Company::inRandomOrder()->first();
        $subject = $this->faker->sentence();
        $thread = MessageService::createThread($subject, $user->account_id, $company->account_id);
        
        $body = $this->faker->sentence();
        MessageService::addMessage($thread, $user->account_id, $body);
        $messages = $thread->messages;
        $this->assertEquals(1, count($messages));
        $this->assertEquals($body, $messages->first()->body);
    }

    public function testCountUnreadMessages()
    {
        $user = User::inRandomOrder()->first();
        $company = Company::inRandomOrder()->first();
        $thread = MessageService::createThread($this->faker->sentence(), $user->account_id, $company->account_id);

        // 2 messages from 1 sender should be counted as 1
        MessageService::addMessage($thread, $user->account_id, $this->faker->sentence());
        MessageService::addMessage($thread, $user->account_id, $this->faker->sentence());
        $this->assertEquals(1, MessageService::countUnreadMessages($company->account_id));
        $this->assertEquals(0, MessageService::countUnreadMessages($user->account_id));

        // get another message from different user
        $user2 = User::where('id', '!=', $user->id)->inRandomOrder()->first();
        $thread2 = MessageService::createThread($this->faker->sentence(), $user2->account_id, $company->account_id);
        MessageService::addMessage($thread2, $user2->account_id, $this->faker->sentence());
        $this->assertEquals(2, MessageService::countUnreadMessages($company->account_id));        
    }

    public function testGetLastMessages()
    {
        $company = Company::inRandomOrder()->first();
        
        $user = User::inRandomOrder()->first();        
        $thread = MessageService::createThread($this->faker->sentence(), $user->account_id, $company->account_id);
        MessageService::addMessage($thread, $user->account_id, $this->faker->sentence());
        MessageService::addMessage($thread, $user->account_id, $this->faker->sentence());

        $user2 = User::where('id', '!=', $user->id)->inRandomOrder()->first();
        $thread2 = MessageService::createThread($this->faker->sentence(), $user2->account_id, $company->account_id);
        MessageService::addMessage($thread2, $user2->account_id, $this->faker->sentence());

        $last_messages = MessageService::getLastMessages($company->account_id);
        $this->assertEquals(2, count($last_messages)); 
    }

    public function testGetUnreadMessagesForUser()
    {
        $from_date = Carbon::now();
        $company = Company::inRandomOrder()->first();
        
        $user = User::inRandomOrder()->first();              
        $thread = MessageService::createThread($this->faker->sentence(), $user->account_id, $company->account_id);
        MessageService::addMessage($thread, $user->account_id, $this->faker->sentence());
        MessageService::addMessage($thread, $user->account_id, $this->faker->sentence());
        $unread_messages = MessageService::getUnreadMessagesForUser($company->account_id, $from_date);
        $sender_ids = $unread_messages->pluck('user_id')->unique();
        $this->assertEquals(2, count($unread_messages));
        $this->assertEquals(1, count($sender_ids));
        $this->assertEquals($user->account_id, $sender_ids->first());

        // read existing messages
        $thread->markAsRead($company->account_id);
        sleep(1);
        
        $unread_messages = MessageService::getUnreadMessagesForUser($company->account_id, Carbon::now());
        $this->assertEquals(0, count($unread_messages));

        // send another message
        $from_date = Carbon::now();
        MessageService::addMessage($thread, $user->account_id, $this->faker->sentence());        
        $unread_messages = MessageService::getUnreadMessagesForUser($company->account_id, $from_date);
        $this->assertEquals(1, count($unread_messages));
    }

    public function testGetAccountsWithMessage()
    {
        // no message
        $from_date = Carbon::now();
        $accounts = MessageService::getAccountsWithMessage($from_date);
        $this->assertEquals(0, count($accounts));

        // add some messages
        $company = Company::inRandomOrder()->first();       
        $user = User::inRandomOrder()->first();              
        $thread = MessageService::createThread($this->faker->sentence(), $user->account_id, $company->account_id);
        MessageService::addMessage($thread, $user->account_id, $this->faker->sentence());
        MessageService::addMessage($thread, $user->account_id, $this->faker->sentence());
        $accounts = MessageService::getAccountsWithMessage($from_date);
        $this->assertEquals(1, count($accounts));

        // replied
        MessageService::addMessage($thread, $company->account_id, $this->faker->sentence());
        $accounts = MessageService::getAccountsWithMessage($from_date);
        $this->assertEquals(2, count($accounts));
    }

    public function testGetAccountsWithLastReadBefore()
    {
        // no thread yet
        $accounts = MessageService::getAccountsWithLastReadBefore(Carbon::now());
        $this->assertEquals(0, count($accounts));

        // add a thread and some messages
        $company = Company::inRandomOrder()->first();       
        $user = User::inRandomOrder()->first();              
        $thread = MessageService::createThread($this->faker->sentence(), $user->account_id, $company->account_id);
        MessageService::addMessage($thread, $user->account_id, $this->faker->sentence());
        MessageService::addMessage($thread, $user->account_id, $this->faker->sentence());
        $accounts = MessageService::getAccountsWithLastReadBefore(Carbon::now());
        $this->assertEquals(1, count($accounts));

        // add another thread
        $company2 = Company::inRandomOrder()->where('id', '!=', $company->id)->first();
        $user2 = User::inRandomOrder()->where('id', '!=', $user->id)->first();
        $thread2 = MessageService::createThread($this->faker->sentence(), $user2->account_id, $company2->account_id);
        MessageService::addMessage($thread2, $user2->account_id, $this->faker->sentence());
        $accounts = MessageService::getAccountsWithLastReadBefore(Carbon::now());
        $this->assertEquals(2, count($accounts));
    }
}
