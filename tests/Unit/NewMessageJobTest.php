<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Jobs\NewMessageJob;
use App\Mail\NewMessage;
use App\Service\MessageService;
use App\Company;
use App\User;
use Carbon\Carbon;
use Mail;

class NewMessageJobTest extends TestCase
{
    public function testHandle()
    {
        Mail::fake();
        $from_date = Carbon::now();
        $company = Company::inRandomOrder()->first();
        $user = User::inRandomOrder()->first();              
        $thread = MessageService::createThread($this->faker->sentence(), $user->account_id, $company->account_id);
        $sentences = $this->faker->sentences(2);
        MessageService::addMessage($thread, $user->account_id, $sentences[0]);
        MessageService::addMessage($thread, $user->account_id, $sentences[1]);
        $this->assertNull($company->account->last_email);

        // run the job
        $job = new NewMessageJob($from_date);
        $job->handle();
        Mail::assertQueued(NewMessage::class, function ($mail) use ($company, $sentences) {
            $messages = array_values($mail->messages)[0]['messages'];
            $body_array = array_reduce($messages, function ($res, $item) {
                $res[] = $item->body;
                return $res;
            }, []);            
            return $mail->hasTo($company->email) 
                && (count(array_keys($mail->messages)) == 1) 
                && (count($messages) == 2)
                && ($body_array == $sentences);
        });

        $company->refresh();
        $this->assertNotNull($company->account->last_email);
    }
}
