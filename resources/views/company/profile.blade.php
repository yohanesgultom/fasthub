@extends('layouts.company.app')

@section('title', __('Employee Profile'))

@section('styles')
<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.css" rel="stylesheet">
@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.js"></script>
<script>
$(document).ready(function() {
    $('.summernote').summernote({
        height: 300,
    });
});
</script>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

            <a href="{{ route('company.home') }}"><< {{ __('Back') }}</a>

            <div class="card">
                <div class="card-header">{{ __('Employee Profile') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>                    
                    @endif

                    @include('partials.formErrors')

                    <form method="POST" action="{{ route('company.update') }}" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group">
                            <label>{{ __('Name') }}</label>
                            <input type="text" name="name" class="form-control" placeholder="Enter your name" value="{{ $company->name }}" required>
                        </div>
                        <div class="form-group">
                            <label>{{ __('Email') }}</label>
                            <input type="email" name="email" class="form-control" placeholder="Enter email" value="{{ $company->email }}" required>
                        </div>
                        <div class="form-group">
                            <label>{{ __('Position') }}</label>
                            <input type="text" name="position" class="form-control" placeholder="Enter your position" value="{{ $company->position }}">
                            <small class="form-text text-muted">{{ __('Example') }}: {{ ('HR Manager, IT Manager, Finance Staff .etc') }}</small>
                        </div>
                        <div class="form-group">
                            <label>{{ __('Company Name') }}</label>
                            <input type="text" name="company_name" class="form-control" placeholder="Enter your company name" value="{{ $company->company_name }}">
                        </div>
                        <div class="form-group">
                            <label>{{ __('Company Domain') }}</label>
                            <input type="text" name="domain" class="form-control" placeholder="Enter your company domain" value="{{ $company->domain }}">
                            <small class="form-text text-muted">{{ __('Example') }}: {{ ('Oil & Gas, Energy, IT, Education, Pharmacy, Health & Medicine, Banking, Finance, Printing/Publishing .etc') }}</small>
                        </div>                        
                        <div class="form-group">
                            <label>{{ __('Company Profile URL') }}</label>
                            <input type="text" name="url" class="form-control" placeholder="Enter URL" value="{{ $company->url }}">
                            <small class="form-text text-muted">{{ __('Example') }}: {{ ('https://fasiloo.com') }}</small>
                        </div>
                        <div class="form-group">
                            <label>{{ __('Logo') }}</label>
                            <input type="file" name="logo" class="form-control">
                            <small class="form-text text-muted">{{ __('Extension: .png, .jpg, or .jpeg Max size: 500 KB') }}</small>
                            <small class="form-text text-muted">{{ __('By uploading logo, you agree to give us permission to display it publicly in our applications') }}</small>
                            @if (!empty($company->logo))
                            <img width="200" src="{{ $company->logo }}"/>
                            @endif                                
                        </div>
                        <div class="form-group">
                            <label>{{ __('Short Profile') }}</label>
                            <small class="form-text text-muted">{{ __('Guide') }}: {{ ('Short description of your company such as business domain, target market, achievements, company size, technology, culture .etc') }}</small>
                            <textarea class="summernote" name="summary">{!! $company->summary !!}</textarea>                            
                        </div>
                        <div class="form-group">
                            <label>{{ __('Share Your Problems') }}</label>
                            <small class="form-text text-muted">{{ __('Guide') }}: {{ ('List of problems that your company needs help with. Each problems may includes brief description, solutions that have you tried/not tried, criteria of facilitator/service that you have in mind .etc') }}</small>
                            <textarea class="summernote" name="problems">{!! $company->problems !!}</textarea>                            
                        </div>                                                
                        <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
