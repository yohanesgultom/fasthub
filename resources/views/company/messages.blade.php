@extends('layouts.company.app')

@section('title', __('Messages'))

@include('partials.messages', [
    'user_type' => 'company',
])