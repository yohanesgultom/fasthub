@extends('layouts.email')

@section('title', 'Verification Email')

@section('content')
<div class="jumbotron text-center">
  <h1>Welcome to {{ config('app.name', 'Fasiloo') }}!</h1>
  <p class="lead">Complete your registration by clicking the button below to verify your email address. If you are having problem with the button, you can also open this URL in your web browser: <a href="{{ route('company.verifyAccount', ['token' => $email_token]) }}">{{ route('company.verifyAccount', ['token' => $email_token]) }}</a></p>
  <p><a class="btn btn-lg btn-success" href="{{ route('company.verifyAccount', ['token' => $email_token]) }}" role="button">Verify Email</a></p>
</div>
@endsection