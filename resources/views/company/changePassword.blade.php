@extends('layouts.company.app')

@section('title', __('Change Password'))

@section('content')
@include('partials.changePassword', ['back_url' => route('company.home'), 'submit_url' => route('company.changePassword')])
@endsection
