@extends('layouts.company.app')

@section('title', __('Send Message'))

@include('partials.chat', [
    'back_url' => route('company.home'),
    'reply_url' => route('company.reply', ['account_id' => $receiver->id]),
])