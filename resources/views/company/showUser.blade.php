@extends('layouts.company.app')

@section('title', __('Facilitator Details'))

@section('scripts')
<script src="{{ asset('libs/starrr/starrr.js') }}"></script>
<script src="{{ asset('js/rating.js') }}"></script>
@endsection

@section('styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="{{ asset('css/rating.css') }}">
<style>
.card {
    margin: 10px 0px;
}
.card label {
    color: gray;
}
.review-avatar {
    margin: 0px 10px;
}
.review-body p {
    font-size: small;
}
.profile .card-body .rating {
    font-size: 2.5em;
    text-decoration: none;
}
</style>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

            <a href="{{ route('company.home') }}"><< {{ __('Back') }}</a>

            <!-- Profile -->
            <div class="card profile">
                <div class="card-header">{{ __('Facilitator Profile') }}</div>

                <div class="card-body">
                    @include('partials.formAlert')

                    <img class="pull-right" height="200" alt="{{ $user->name }}" src="{{ $user->photo ?? '/img/avatar.png' }}"/>
                    <div class="form-group">
                        <label>{{ __('Name') }}</label>
                        <div>{{ $user->name }}</div>
                    </div>
                    <div class="form-group">
                        <label>{{ __('Title') }}</label>
                        <div>{{ $user->title }}</div>
                    </div>
                    <div class="form-group">
                        <label>{{ __('Email') }}</label>
                        <div>{{ $user->email }}</div>
                    </div>
                    <div class="form-group">
                        <label>{{ __('Competencies') }}</label>
                        <div>
                            @foreach ($user->skills as $s)
                            <span class="badge badge-info">{{ $s->name }}</span>
                            @endforeach
                        </div>
                    </div>
                    <div class="form-group">
                        <label>{{ __('Average Rating') }}</label>
                        <div class="rating rating-view" data-score="{{ $user->accepted_reviews->avg('score') }}"></div>
                    </div>                    
                    <div class="form-group">
                        <label>{{ __('Summary') }}</label>
                        <div>{!! $user->summary !!}</div>
                    </div>                    
                    <div class="form-group">
                        <label>{{ __('Education & Certification') }}</label>
                        <div>{!! $user->education !!}</div>
                    </div>                    
                    <div class="form-group">
                        <label>{{ __('Experience') }}</label>
                        <div>{!! $user->experience !!}</div>
                    </div>                    
                </div>
            </div>

            <!-- Submit Review -->
            @include('partials.formReview', ['url' => route('company.review', ['account_id' => $user->account_id])])

            <!-- Reviews -->
            @include('partials.reviews')
        </div>
    </div>
</div>
@endsection
