@extends('layouts.company.app')

@section('title', __('Employee Home'))

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.min.js"></script>
<script src="{{ asset('libs/starrr/starrr.js') }}"></script>
<script src="{{ asset('js/rating.js') }}"></script>
<script>
$(document).ready(function() {
    $('.tagsinput').tagsinput({
        tagClass: 'badge badge-info',
        trimValue: true,
        maxChars: 100,
        maxTags: 30,
        confirmKeys: [13, 188],
    });
    $('.bootstrap-tagsinput input').on('keypress', function(e) {
        if (e.keyCode == 13){
            e.keyCode = 188;
            e.preventDefault();      
        };
    });
});        
</script>
@endsection

@section('styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="{{ asset('css/rating.css') }}">
<style>
.card {
    margin: 10px 0px;
}
.card label {
    color: gray;
}
.card-body img {
    margin-bottom: 10px;
}
.rating-view {
    font-size: 1.5em;
}
.bootstrap-tagsinput {
    display: block;
}
</style>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{__('Employee Dashboard')}}</div>

                <div class="card-body">
                    @include('partials.formAlert')
                    
                    <div class="row">
                        <div class="col-lg-3 col-6 mb-1">
                            <a class="btn btn-block btn-success" href="{{ route('company.showMessagesForm') }}">
                                <i class="fa fa-comments" style="font-size:28pt"></i><br> 
                                <small class="{{ $new_message_count > 0 ? 'alert-label' : ''}}">
                                    {{ __('Messages') }} 
                                    {{ $new_message_count > 0 ? '('.$new_message_count.' '.__('new').')' : '' }}
                                </small>
                            </a>
                        </div>
                        <div class="col-lg-3 col-6 mb-1">
                            <a class="btn btn-block btn-info" href="{{ route('company.update') }}">
                                <i class="fa fa-building" style="font-size:28pt"></i><br> 
                                <small>{{ __('Edit profile') }}</small>
                            </a>
                        </div>
                        <div class="col-lg-3 col-6 mb-1">
                            <a class="btn btn-block btn-warning" href="{{ route('company.showChangePassword') }}">
                                <i class="fa fa-key" style="font-size:28pt"></i><br> 
                                <small>{{ __('Change Password') }}</small>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <h3>{{__('Facilitators')}}</h3>

            <div class="card">
                <div class="card-header">{{__('Search')}}</div>

                <div class="card-body">
                    <form method="GET" action="">
                        <div class="form-group">
                            <label>{{ __('Filter by') }} {{ __('Competencies') }}</label>
                            <input type="text" name="skills" class="form-control tagsinput" placeholder="Enter competencies or skills" value="{{ $skills }}" >
                            <small class="form-text text-muted">{{ __('Guide: Press ENTER after each item') }} {{ __('Examples') }} : internet marketing, human capital, growth hacking, agile software development</small>
                        </div>
                        <button type="submit" class="btn btn-success">
                                <i class="fa fa-search mr-1"></i>
                                {{ __('Filter') }}
                        </button>
                    </form>        
                </div>
            </div>

            <br>

            {{ $facilitators->appends(app('request')->except('page'))->links() }}

            @if (count($facilitators) <= 0)
            <div class="alert alert-info">{{ __('No data yet. Please check again later') }}</div>
            @endif

            @foreach ($facilitators as $f)
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-user mr-1"></i>
                    {{ $f->name }} | 
                    <label><em>{{ $f->title }}</em></label>
                </div>
                <div class="card-body">
                    <img class="float-md-right" height="150" alt="{{ $f->name }}" src="{{ $f->photo ?? '/img/avatar.png' }}"/>
                    <div class="form-group">
                        <label>{{ __('Email') }}</label>
                        <div>{{ $f->email }}</div>
                    </div>
                    <div class="form-group">
                        <label>{{ __('Competencies') }}</label>
                        <div>
                            @foreach ($f->skills as $s)
                            <span class="badge badge-info">{{ $s->name }}</span>
                            @endforeach
                        </div>
                    </div>                    
                    <div class="form-group">
                        <label>{{ __('Average Rating') }}</label>
                        <div class="rating rating-view" data-score="{{ $f->accepted_reviews->avg('score') }}"></div>
                    </div>
                    <div class="form-group">
                        <label>{{ __('Summary') }}</label>
                        <div>{{ str_limit(strip_tags($f->summary), 100, '...') }}</div>
                    </div>
                    <a href="{{ route('company.showUser', ['id' => $f->id]) }}" class="btn btn-primary btn-details">
                        <i class="fa fa-search"></i>
                        {{ __('Details') }}
                    </a>
                    <a href="{{ route('company.chat', ['account_id' => $f->account_id ]) }}" class="btn btn-success btn-chat">
                        <i class="fa fa-comments-o"></i>
                        {{ __('Send Message') }}
                    </a>
                </div>
            </div>
            @endforeach

            {{ $facilitators->appends(app('request')->except('page'))->links() }}
        </div>
    </div>
</div>
@endsection
