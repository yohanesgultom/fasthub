@extends('layouts.public')

@section('title', __('Terms and Conditions'))

@section('content')

<div class="position-relative overflow-hidden p-3 p-md-5 m-md-5 mt-5 bg-light">        

    <h1 class="text-center">{{ __('Terms and Conditions') }}</h1>

    <h3 class="mb-10">Definisi</h3>
    
    <p>Di samping persyaratan yang tertera dalam Perjanjian ini, definisi berikut juga berlaku di seluruh Perjanjian ini</p><p>"Fasilitator" adalah setiap orang atau lembaga yang melakukan registrasi dimenu fasilitator</p><p><span>"Karyawan" adalah setiap orang atau lembaga/organisasi/perusahaan yang melakukan registrasi dimenu perusahaan</span></p>
    
    <h3 class="mb-10">Persyaratan umum</h3>
        
    <p>Perjanjian ini tunduk dan diatur dalam Persyaratan Umum ('Syarat dan Ketentuan'). Apabila anda melakukan registrasi, berarti anda menyatakan bahwa telah membaca dan dengan ini menerima syarat dan ketentuan.</p>
    <p>&nbsp;</p>
    <p>1. Informasi Fasilitator atau Karyawan</p>
    <p>1.1 Informasi yang disediakan oleh Fasilitator atau Karyawan untuk ditampilkan di {{ config('app.name') }} &nbsp;harus menyertakan informasi yang berkaitan dengan Fasilitator atau Karyawan harus mematuhi semua format dan standar yang diberikan oleh {{ config('app.name') }}. {{ config('app.name') }} &nbsp;berhak mengubah atau menghapus informasi apa pun saat mengetahui bahwa informasi tersebut salah atau tidak lengkap atau melanggar syarat dan ketentuan dalam Perjanjian ini.</p>
    <p>1.2 Fasilitator atau Karyawan menyatakan dan menyetujui bahwa Informasi Fasilitator atau Karyawan selalu benar, akurat, dan tidak menyesatkan. Fasilitator atau Karyawan selalu bertanggung jawab atas pernyataan yang benar dan terkini dalam. Termasuk informasi yang diberikan diluar dari platform yang ditampilkan oleh {{ config('app.name') }} misalnya melalui telepon, email, whatsapp, dll.</p>
    <p>1.3 Informasi yang disediakan oleh Fasilitator atau Karyawan untuk Platform akan tetap menjadi properti eksklusif &nbsp;Fasilitator atau Karyawan. Informasi yang diberikan oleh &nbsp;Fasilitator atau Karyawan dapat diedit atau diubah oleh {{ config('app.name') }} &nbsp;dan selanjutnya diterjemahkan ke dalam bahasa lain, di mana terjemahannya tetap menjadi properti eksklusif {{ config('app.name') }}. Konten yang diedit dan diterjemahkan akan dipergunakan secara khusus oleh {{ config('app.name') }} &nbsp;di Platform dan tidak boleh digunakan (dengan cara atau bentuk apa pun) oleh Fasilitator atau Karyawan untuk distribusi atau jalur penjualan maupun tujuan lain. Dilarang mengubah atau memperbarui informasi deskriptif Fasilitator atau Karyawan kecuali dengan persetujuan tertulis dari {{ config('app.name') }}.</p>
    <p>1.4 {{ config('app.name') }} &nbsp;tidak bertanggung jawab tidak bertanggung jawab atas kebenaran dan kelengkapan informasi, foto ataupun hasil &ldquo;review&rdquo; yang diberikan oleh Fasilitator atau Karyawan yang ada didalam website {{ config('app.name') }} &nbsp;maupun diluar website {{ config('app.name') }}. Apabila ada ketidak benaran informasi yang diberikan, {{ config('app.name') }} &nbsp; tidak dapat dituntut secara hukum dan tidak memiliki kewajiban untuk terlibat didalam pertikaian yang terjadi.</p>
    <p>&nbsp;</p>
    <p>2. Reservasi Antara Fasilitator Dengan Karyawan</p>
    <p>2.1 {{ config('app.name') }} &nbsp;tidak bertanggung jawab terhadap kesepakatan dan perjanjian yang dilakukan antara Fasilitator dan Karyawan. {{ config('app.name') }} terbebas dari segala keterlibatan terhadap pelanggaran dan konsukuensi dari kesepakatan maupun perjanjian yang terjadi antara Fasilitator dan Karyawan</p>
    <p>2.2 Keluhan atau klaim berkenaan dengan (produk atau layanan yang ditawarkan, diadakan atau disediakan oleh) Fasilitator atau permintaan khusus yang diajukan oleh Karyawan akan ditangani oleh Fasilitator, tanpa perantaraan atau campur tangan {{ config('app.name') }}. {{ config('app.name') }} tidak bertanggung jawab dan menyangkal segala bentuk tanggung jawab yang berkenaan dengan klaim dari Karyawan.</p>
    <p>2.3 Atas kemauannya sendiri, {{ config('app.name') }} dapat kapan saja (a) menawarkan layanan (dukungan) pelanggan bagi Karyawan, (b) bertindak sebagai penengah antara Karyawan dan Fasilitator, (c) menyediakan alternatif Fasilitator dengan standar yang sama atau lebih baik jika terjadi kelebihan reservasi atau penyimpangan yang dilakuakan oleh Fasilitator.</p>
    <p>2.4 Fasilitator harus memastikan bahwa syarat dan ketentuan pembayaran diuraikan dengan jelas kepada Karyawan sebelum terjadi kesepakatan atau perjanjian kerja sama.</p>
    <p>2.5 Fasilitator bertanggung jawab untuk menagih pembayaran jasa/produk kepada Karyawan. Termasuk biaya pembatalan yang dikenai biaya (Termasuk Pajak apabila diperlukan ). {{ config('app.name') }} &nbsp;tidak bertanggung jawab terhadap semua jenis masalah pembayaran antara Fasilitator dan Karyawan.</p>
    <p>2.6 Pemasaran Langsung ke Tamu</p>
    <p>Fasilitator setuju untuk tidak secara khusus menjadikan Karyawan yang diperoleh melalui {{ config('app.name') }} sebagai sasaran promosi pemasaran online maupun offline atau surat yang diminta maupun tidak diminta (SPAM).</p>
    <p>&nbsp;</p>
    <p>3. Izin / Lisensi</p>
    <p>3.1 Dengan ini pihak Karyawan dan Fasilitator memberikan hak dan lisensi noneksklusif, bebas royalti, dan berskala global kepada {{ config('app.name') }} (atau sublisensi bila perlu):</p>
    <p><span style="display: block; float: left; min-width: NaNcm;">(a)&nbsp;</span>untuk menggunakan, mereproduksi, meminta untuk direproduksi, menyebarkan, memberikan sublisensi, menyampaikan dan menyediakan dengan cara apa pun, dan menampilkan elemen yang telah disepakati dari Hak Kekayaan Intelektual Akomodasi sebagaimana diberikan kepada {{ config('app.name') }} oleh Karyawan dan Fasilitator menurut Perjanjian ini dan yang diperlukan oleh {{ config('app.name') }} untuk menjalankan hak dan kewajibannya menurut Perjanjian ini&nbsp;</p>
    <p><span style="display: block; float: left; min-width: NaNcm;">(b)&nbsp;</span>menggunakan, mereproduksi, meminta untuk direproduksi, mendistribusikan, memberikan sublisensi, menampilkan dan menggunakan (termasuk tetapi tidak terbatas pada melakukan, memodifikasi, mengadaptasi, menyampaikan, mereproduksi, menyalin dan menyediakan kepada publik dengan cara apa pun) Informasi Fasilitator dan Karyawan.&nbsp;</p>
    <p><span style="display: block; float: left; min-width: NaNcm;">(c)&nbsp;</span>{{ config('app.name') }} tidak bertanggung jawab terhadap pemasangan lambang, logo, image, ataupun foto yang dilakuan oleh pihak atau orang yang tidak layak secara hukum melakukan pemasangan tersebut&nbsp;</p>
    <p>3.2 {{ config('app.name') }} dapat memberikan sublisensi, menyediakan, memberitahukan, dan menawarkan Informasi Fasilitator dan Karyawan (termasuk Hak Kekayaan Intelektual yang terkait) dan penawaran istimewa yang diberikan oleh Fasilitator di Platform serta hak dan lisensi lebih lanjut sebagaimana tercantum dalam Perjanjian ini melalui atau atas kerja sama dengan (situs web, aplikasi, platform, alat-alat, atau perangkat lainnya milik) perusahaan afiliasi dan/atau pihak ketiga ("Platform Pihak Ketiga").</p>
    <p>3.3 {{ config('app.name') }} tidak akan pernah berkewajiban kepada Fasilitator dan Karyawan atas segala bentuk tindakan atau penghapusan pada bagian Platform Pihak Ketiga mana pun.</p>
    <p>&nbsp;</p>
    <p><span style="display: block; float: left; min-width: NaNcm;">4.</span>Ulasan Fasilitator dan Karyawan&nbsp;</p>
    <p>4.1 Pihak Fasilitator dan Karyawan tidak akan membuat klaim apapun terhadap {{ config('app.name') }} terkait dengan Peringkat (bintang) Fasilitator dan</p>
    <p>4.2 {{ config('app.name') }} berhak menampilkan komentar dan nilai &nbsp;Fasilitator dan Karyawan diPlatform. &nbsp;Fasilitator dan Karyawan membenarkan bahwa {{ config('app.name') }} hanyalah penyebar (tanpa kewajiban untuk membuktikan) dan bukan penerbit komentar-komentar ini.</p>
    <p>4.3 {{ config('app.name') }} akan berupaya semaksimal mungkin untuk memantau dan meninjau komentar Fasilitator dan Karyawan dalam kaitannya dengan kesopanan atau etika berkomunikasi. {{ config('app.name') }} berhak untuk menolak, mengedit, atau menghapus komentar yang tidak baik apabila komentar tersebut memuat ketidaksopanan atau melanggar hukum.</p>
    <p>4.4 {{ config('app.name') }} tidak akan melakukan diskusi, perundingan, ataupun korespondensi dengan pihak Fasilitator dan Karyawan berkenaan dengan (isi, atau konsekuensi publikasi atau penyebaran) komentar.</p>
    <p>4.5 {{ config('app.name') }} tidak memiliki dan menyangkal segala bentuk kewajiban dan tanggung jawab atas isi dan konsekuensi dari (publikasi atau penyebaran) segala bentuk komentar atau penilaian dari siapa pun atau dalam bentuk apa pun.</p>
    <p>4.3 Iklan PPC dan Pemasaran (Online)</p>
    <p>4.3.1 {{ config('app.name') }} berhak mempromosikan Fasilitator dan Karyawan dengan menggunakan nama(-nama) Fasilitator dan Karyawan dalam pemasaran online, termasuk pemasaran melalui email dan/atau iklan bayar per klik (PPC). {{ config('app.name') }} menjalankan kampanye pemasaran online atas biaya dan kehendaknya sendiri.</p>
    <p>&nbsp;</p>
    <p>5. Pernyataan dan Jaminan&nbsp;</p>
    <p><span style="display: block; float: left; min-width: NaNcm;">5.1&nbsp;</span>Pihak Fasilitator dan Karyawan tidak dengan cara apapun terhubung ke, bagian dari, terlibat atau terkait dengan atau berada dalam kendali, manajemen atau kepemilikan dari:&nbsp;</p>
    <p>(a) teroris atau organisasi teroris;</p>
    <p><span style="display: block; float: left; min-width: NaNcm;">(b)&nbsp;</span>pihak / orang yang terdaftar sebagai badan/orang yang ditunjuk negara atau badan / orang yang dilarang, dan&nbsp;</p>
    <p><span style="display: block; float: left; min-width: NaNcm;">(c)&nbsp;</span>pihak / orang yang melakukan tindak pidana pencucian uang, penyuapan, penipuan atau korupsi.&nbsp;</p>
    <p><span style="display: block; float: left; min-width: NaNcm;">5.2&nbsp;</span>Setiap Pihak menyatakan dan menjamin kepada Pihak lainnya bahwa selama berlakunya Perjanjian ini, setiap Pihak harus mematuhi seluruh undang-undang, kode, regulasi, peraturan dan tata tertib negara&nbsp;</p>
    <p>5.3 {{ config('app.name') }} tidak bertanggung jawab dan tidak memiliki kewajiban dalam kaitannya dengan kegiatan atau aktivitas yang terjadi antara Fasilitator dan Karyawan</p>
    <p>&nbsp;</p>
    <p>6. Ganti Rugi dan Pertanggungjawaban</p>
    <p>6.1 Fasilitator dan Karyawan akan mengganti rugi, memberikan kompensasi penuh, dan membebaskan {{ config('app.name') }} dari atau terhadap segala bentuk liabilitas, biaya, pengeluaran (termasuk, tetapi tidak terbatas pada, biaya dan pengeluaran yang wajar untuk pengacara), kerusakan, kerugian, kewajiban, klaim dalam bentuk apa pun, bunga, penalti, dan proses hukum yang dibayarkan, diderita atau dialami oleh {{ config('app.name') }} (atau direksi, pejabat, karyawan, agen, perusahaan afiliasi, dan sub-kontraktornya) terkait dengan:</p>
    <p><span style="display: block; float: left; min-width: NaNcm;">(i)&nbsp;</span>seluruh klaim yang dibuat oleh Fasilitator dan Karyawan terkait Informasi yang tidak akurat, salah, atau menyesatkan di Platform;&nbsp;</p>
    <p><span style="display: block; float: left; min-width: NaNcm;">(ii)&nbsp;</span>seluruh klaim mengenai pelanggaran, penipuan, kesalahan tindakan jahat yang disengaja ataupun tidak disengaja, kelalaian atau pelanggaran kesepakatan atau perjanjian, pembayaran, pengembalian uang atau penagihan uang.&nbsp;</p>
    <p>&nbsp;</p>
    <p>7. Pajak</p>
    <p>7.1 Pihak Fasilitator bertanggung jawab untuk memotong dan melaporkan pajak sesuai dengan peraturan pajak yang berlaku serta praktek dan permintaan dari otoritas pajak. Pihak Fasilitator harus menanggung dan bertanggung jawab terhadap pembayaran dan penyetoran pajak yang berlaku pada Komisi (pembayaran) dan bunga akibat keterlambatan pembayaran dan denda yang dikenakan oleh pihak otoritas pajak.</p>
    <p>8.&nbsp; Keadan Kahar / Force Majeure</p>
    <p><span style="display: block; float: left; min-width: NaNcm;">8.1&nbsp;</span>PARA PIHAK dibebaskan dari tanggung-jawab atas pelaksanaan kewajibannya, jika kegagalan dalam melaksanakan kewajibannya itu disebabkan oleh peristiwa yang disebabkan karenakuasa di luar kehendak (force majeure) kendali PIHAK yang bersangkutan termasuk tetapi tidak terbatas pada: huru-hara, perang, pemberontakan, bencana alam, epidemi &nbsp;dan sebab-sebab lain di luar kekuasaan PARA PIHAK.&nbsp;</p>
    <p><span style="display: block; float: left; min-width: NaNcm;">8.2&nbsp;</span>PIHAK yang terkena force majeure wajib memberitahukan keadannya kepada PIHAK lainnya dalam perjanjian paling lambat 1 (satu) hari sejak terjadinya force majeure. Apabila PIHAK tersebut melanggar ketentuan ini, maka dianggap tidak terkena force majeure dan tetap wajib untuk memenuhi kewajibannya dalam Perjanjian ini.&nbsp;</p>
</div>

@endsection