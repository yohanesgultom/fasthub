@extends('layouts.public')

@section('title', __('Home'))

@section('content')

<!-- Start Banner Area -->
<div class="position-relative overflow-hidden p-3 p-md-5 mt-5 text-center bg-light">
    <div class="col-md-5 p-lg-5 mx-auto my-5">
        <h1 class="display-3 font-weight-normal">
            <img height="64" src="{{ asset('favicon.png') }}"/>
            {{ config('app.name', 'Laravel') }}
        </h1>
        <p class="lead font-weight-normal">Share your experience, get your revenue<br>Share your problem, get your solution</p>
    </div>
</div>
    
<div class="d-md-flex flex-md-equal w-100 my-md-3 pl-md-3">    

    <!-- Facilitator -->
    <div class="bg-white mr-md-3 pt-3 px-3 pt-md-5 px-md-5 pb-3 text-center overflow-hidden">
        <div class="my-3 py-3">
            <h2 class="display-5">Anda membutuhkan penghasilan tambahan?</h2>
            <p class="lead">Anda punya pengalaman menyelesaikan masalah saat anda bekerja?</p>
        </div>
        <div id="howto-facilitator" class="mx-auto text-left">
            <div class="row">
                <div class="col-md-1 col-2">
                    <span class="hiw-number">1</span>
                </div>
                <div class="col-md-11 col-10">
                    <h5 class="text-uppercase">Daftarkan diri Anda sebagai fasilitator</h5>
                    <p class="text-muted"><a href="{{ route('register') }}">Daftarkan</a> diri Anda atau <a href="{{ route('login') }}">masuk</a> untuk memulai</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-1 col-2">
                    <span class="hiw-number">2</span>
                </div>
                <div class="col-md-11 col-10">
                    <h5 class="text-uppercase">Cari masalah Karyawan yang sesuai</h5>
                    <p class="text-muted"><a href="{{ route('user.home') }}">Cari</a> karyawan di perusahaan lain yang membutuhkan pengalaman Anda</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-1 col-2">
                    <span class="hiw-number">3</span>
                </div>
                <div class="col-md-11 col-10">
                    <h5 class="text-uppercase">Perkenalkan diri Anda</h5>
                    <p class="text-muted">Kirim pesan pada karyawan tersebut untuk memperkenalkan diri dan membagikan pengalaman Anda</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-1 col-2">
                    <span class="hiw-number">4</span>
                </div>
                <div class="col-md-11 col-10">
                    <h5 class="text-uppercase">Dapatkan penghasilan tambahan dengan waktu yang fleksibel!</h5>
                    <p class="text-muted">Jadwalkan pertemuan sesuai kondisi waktu Anda, dan tentukan harga jasa layanan Anda</p>
                </div>
            </div>
            <div class="text-center">
                <a href="{{ route('register') }}" class="btn btn-primary">
                    <i class="fa fa-user mr-1"></i> 
                    {{ __('Start Now') }}
                </a>
            </div>
        </div>
    </div>

    <!-- Company-->
    <div class="bg-white mr-md-3 pt-3 px-3 pt-md-5 px-md-5 pb-3 text-center overflow-hidden">
        <div class="my-3 p-3">
            <h2 class="display-5">Anda memiliki kesulitan dalam mengatasi masalah pada saat bekerja?</h2>
        </div>
        <div id="howto-company" class="mx-auto text-left">
            <div class="row">
                <div class="col-md-1 col-2">
                    <span class="hiw-number">1</span>
                </div>
                <div class="col-md-11 col-10">
                    <h5 class="text-uppercase">Daftarkan diri Anda</h5>
                    <p class="text-muted"><a href="{{ route('company.register') }}">Daftarkan</a> diri Anda sebagai Karyawan (<em>Employee</em>) atau <a href="{{ route('company.login') }}">masuk</a> untuk memulai</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-1 col-2">
                    <span class="hiw-number">2</span>
                </div>
                <div class="col-md-11 col-10">
                    <h5 class="text-uppercase">Cari fasilitator</h5>
                    <p class="text-muted"><a href="{{ route('company.home') }}">Cari Fasilitator</a> dengan kompetensi dan pengalaman yang sesuai degan kebutuhan Anda</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-1 col-2">
                    <span class="hiw-number">3</span>
                </div>
                <div class="col-md-11 col-10">
                    <h5 class="text-uppercase">Diskusikan kebutuhan Anda</h5>
                    <p class="text-muted">Kirim pesan untuk mulai berdiskusi dengan Fasilitator tentang masalah Anda</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-1 col-2">
                    <span class="hiw-number">4</span>
                </div>
                <div class="col-md-11 col-10">
                    <h5 class="text-uppercase">Temukan solusi masalah Anda!</h5>
                    <p class="text-muted">Jadwalkan pertemuan dengan Fasilitator yang sesuai untuk mendapatkan solusi Anda</p>
                </div>
            </div>
            <div class="text-center">
                <a href="{{ route('company.register') }}" class="btn btn-info">
                    <i class="fa fa-building mr-1"></i> 
                    {{ __('Start Now') }}
                </a>
            </div>            
        </div>
    </div>
</div>

<!-- Contact Us -->
<div id="contact-us" class="position-relative overflow-hidden p-3 p-md-5 mt-5 text-white bg-dark">
    <div class="row">
        <div class="col-md-6 px-lg-5 text-center">
            <h2 class="display-5">Hubungi Kami</h2>
            <p class="lead text-center">
                Anda membutuhkan bantuan?<br>
                Ada masalah yang ingin dilaporkan?<br>
                Atau ada saran yang Anda sampaikan?
            </p>
            <p class="text-justify">
                Atau hanya ingin sekedar menyapa dan memberi kami semangat? 😊 Silahkan gunakan form berikut ini. Kami akan sangat senang menerima pesan Anda dan akan sesegera mungkin memberikan respon.
            </p>
        </div>
        <div class="col-md-6">
            @include('partials.formAlert')
            
            <form method="POST" action="{{ route('contactUs') }}">
                @csrf

                <div class="form-group">
                    <label>{{ __('Email') }}</label>
                    <input type="email" class="form-control" name="email" aria-describedby="emailHelp" placeholder="Enter your email" required>
                    <small class="form-text text-muted">{{ __('We will use this to follow up your issue') }}</small>
                </div>
                <div class="form-group">
                    <label>{{ __('Message') }}</label>
                    <textarea class="form-control" rows="10" maxlength="1000" name="message" placeholder="Write your questions, feedbacks, critics, rants here" required></textarea>
                </div>
                <div class="mb-3 px-2 px-lg-0">
                    {!! Recaptcha::render() !!}
                    @if ($errors->has('g-recaptcha-response'))
                    <span class="invalid-feedback" style="display:block">
                        <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                    </span>
                    @endif                            
                </div>                
                <button type="submit" class="btn btn-primary">
                    <i class="fa fa-paper-plane"></i> {{ __('Send') }}
                </button>
            </form>
        </div>
    </div>
</div>

@endsection