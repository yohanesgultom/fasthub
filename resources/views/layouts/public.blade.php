
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="{{ config('app.name', 'Laravel') }}">
    <meta name="author" content="{{ config('app.name', 'Laravel') }}">
    <link rel="shortcut icon" href="{{ asset('favicon.png') }}" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title') | {{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">    
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" type="text/css">

    <link href="{{ asset('css/public.css') }}" rel="stylesheet">

    @include('partials.googleAnalytics')
  </head>

  <body>
    <header>
        <!-- Fixed navbar -->
        <nav class="navbar navbar-light navbar-expand-md fixed-top bg-white">
            <div class="container">
                <a class="navbar-brand" href="{{ url('') }}">
                    <img height="32" src="{{ asset('favicon.png') }}"/>
                    {{ config('app.name', 'Laravel') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarCollapse">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link active" href="{{ route('login') }}"> 
                                <i class="fa fa-user-o mr-1"></i> 
                                {{ __('Facilitator') }}
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" href="{{ route('company.login') }}">
                                <i class="fa fa-building-o mr-1"></i> 
                                {{ __('Employee') }}
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active"  href="{{ route('terms-conditions') }}">                                    
                                <i class="fa fa-exclamation-circle mr-1"></i>
                                {{ __('Terms & Conditions') }}
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>

    @yield('content')

    <footer class="footer bg-white">
        <div class="float-right"><a href="#">{{ __('Back to top') }}</a></div>
        <div>© 2018 {{ config('app.name', 'Laravel') }} · <a href="{{ route('terms-conditions') }}">{{ __('Terms & Conditions') }}</a></div>
    </footer>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/js/bootstrap.min.js"></script>
  </body>
</html>
