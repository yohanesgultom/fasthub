<div class="chat-message media {{ $receiver->id == $message->user->id ? 'chat-reply' : '' }}">
    <a class="chat-avatar pull-left" href="#">
        <img src="{{ '/img/avatar.png' }}" alt="{{ $message->user->name }}" height="50">
    </a>
    <div class="chat-body media-body">
        <small class="chat-name media-heading">{{ $message->user->name }}</small>
        <div class="chat-message">{!! $message->body !!}</div>
        <div class="chat-info text-muted">
            <small>{{ $message->created_at->diffForHumans() }}</small>
        </div>
    </div>
</div>