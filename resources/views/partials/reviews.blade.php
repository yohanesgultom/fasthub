<!-- Reviews -->
<div class="card">
    <div class="card-header">{{ __('Reviews') }}</div>

    <div class="card-body">
        @foreach ($reviews as $r)
        <div class="review-message media">
            <a class="review-avatar pull-left" href="#">
                <img src="{{ '/img/avatar.png' }}" alt="{{ $r->reviewer->name }}" height="50">
            </a>
            <div class="review-body media-body">
                <small class="mute">{{ $r->created_at->diffForHumans() }}</small>
                <div class="rating rating-view" data-score="{{ $r->score }}"></div>                
                <blockquote class="blockquote">
                    <p class="mb-0">{{ $r->body }}</p>
                    <footer class="blockquote-footer">{{ $r->reviewer->name }}</footer>                                        
                </blockquote>                
            </div>
        </div>
        @endforeach

        {{ $reviews->links() }}
    </div>
</div>