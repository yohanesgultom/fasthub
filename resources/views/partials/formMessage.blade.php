<form method="POST" action="{{ $url }}">
    @csrf
    <div class="form-group">
        <textarea class="summernote" name="message"></textarea>
    </div>
    <button type="submit" class="btn btn-primary">{{ __('Send') }}</button>
</form>