<script async src="https://www.googletagmanager.com/gtag/js?id={{ config('app.analytics_id') }}"></script>
<script>
    let analyticsId = "{{ config('app.analytics_id') }}";
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', analyticsId);
</script>    
