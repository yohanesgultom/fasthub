<div class="card">
    <div class="card-header">{{ __('Submit Review') }}</div>
    <div class="card-body">
        @include('partials.formErrors')        
        <form method="POST" action="{{ $url }}">
            @csrf
            <div class="form-group">
                <label>{{ __('Rating') }}</label>
                <div class="rating rating-input"></div>
                <input type="hidden" name="score"/>
            </div>
            <div class="form-group">
                <label>{{ __('Review') }}</label>
                <textarea class="form-control" rows="3" name="body"></textarea>
            </div>
            <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
        </form>
    </div>
</div>
