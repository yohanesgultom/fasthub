<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

            <a href="{{ $back_url }}"><< {{ __('Back') }}</a>
            
            <div class="card">
                <div class="card-header">{{ __('Change Password') }}</div>

                <div class="card-body">
                    @include('partials.formAlert')
                    @include('partials.formErrors')

                    <form method="POST" action="{{ $submit_url }}">
                        @csrf

                        <div class="form-group">
                            <label>{{ __('Current password') }}</label>
                            <input type="password" name="password" class="form-control" placeholder="{{ __('Current password') }}" required>
                        </div>
                        <div class="form-group">
                            <label>{{ __('New password') }}</label>
                            <input type="password" name="password_new" class="form-control" placeholder="{{ __('New password') }}" required>
                        </div>
                        <div class="form-group">
                            <label>{{ __('Confirm new password') }}</label>
                            <input type="password" name="password_new_confirmation" class="form-control" placeholder="{{ __('Confirm new password') }}" required>
                        </div>                        
                        <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
