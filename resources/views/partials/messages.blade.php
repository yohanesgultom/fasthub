@section('styles')
<style>
.card {
    margin: 10px 0px;
}
.card label {
    color: gray;
}
.chat-avatar img {
    margin: 10px;
}
.chat-message a {
    text-decoration: none;
}
.chat-message-body {
    padding: 5px 0px;
    font-size: 9pt;
}
.chat-message-body .chat-message-sender {
    font-weight: bolder;
}
</style>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            
            <a href="{{ route($user_type.'.home') }}"><< {{ __('Back') }}</a>

            <!-- Messages -->
            <div class="card">
                <div class="card-header">{{ __('Messages') }}</div>

                <div class="card-body">
                    @include('partials.formAlert')

                    @if (count($messages) == 0)
                    <div class="alert alert-warning">{{ __('Sorry, no message yet') }}</div>
                    @endif

                    @foreach ($messages as $m)
                    <div class="chat-message media">
                        <a class="chat-avatar pull-left" href="{{ route($user_type.'.chat', ['account_id' => $m->receiver_id ]) }}">
                            <img src="{{ $m->receiver_avatar ?? '/img/avatar.png' }}" alt="{{ $m->receiver_name }}" height="50">
                        </a>
                        <a href="{{ route($user_type.'.chat', ['account_id' => $m->receiver_id ]) }}">
                            <div class="chat-body media-body">                            
                                <small class="chat-name media-heading">{{ __('Chat with') }} {{ $m->receiver_name }}</small>
                                <div class="chat-message-body text-muted">
                                    <span class="chat-message-sender">{{ $m->name }}</span> : <span>{{ str_limit(strip_tags($m->body), 80) }}</span>
                                </div>
                                <div class="chat-info text-muted">
                                    <small>{{ $m->created_at->diffForHumans() }}</small>
                                </div>
                            </div>
                        </a>
                    </div>
                    @endforeach
                </div>
            </div>

        </div>
    </div>
</div>
@endsection
