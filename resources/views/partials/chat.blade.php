@section('styles')
<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.css" rel="stylesheet">
<style>
.card {
    margin: 10px 0px;
}
.card label {
    color: gray;
}
.chat-avatar img {
    margin: 10px;
}
.chat-message {
    padding: 5px 0px;
}
.chat-reply {
    background-color: #ecf2f5;
}
</style>
@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.js"></script>
<script>
$(document).ready(function() {
    $('.summernote').summernote({
        height: 150,
        toolbar: [
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']]
        ]        
    });
});
</script>
@endsection


@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            
            <a href="{{ $back_url }}"><< {{ __('Back') }}</a>

            <!-- Messages -->
            <div class="card">
                <div class="card-header">{{ $thread->subject }} {{ __('with') }} {{ $receiver->name }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @elseif (session('error'))
                        <div class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                    @endif

                    @foreach ($thread->messages as $message)
                        @include('partials.message')
                    @endforeach
                </div>
            </div>

            <!-- Send Message Form -->
            <div class="card">
                <div class="card-header">{{ __('Send Message') }}</div>
                <div class="card-body">
                    @include('partials.formMessage', ['url' => $reply_url])
                </div>
            </div>

        </div>
    </div>
</div>
@endsection
