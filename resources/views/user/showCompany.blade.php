@extends('layouts.company.app')

@section('title', __('Company Details'))

@section('scripts')
<script src="{{ asset('libs/starrr/starrr.js') }}"></script>
<script src="{{ asset('js/rating.js') }}"></script>
@endsection

@section('styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="{{ asset('css/rating.css') }}">
<style>
.card {
    margin: 10px 0px;
}
.card label {
    color: gray;
}
.review-avatar {
    margin: 0px 10px;
}
.review-body p {
    font-size: small;
}
.profile .card-body .rating {
    font-size: 2.5em;
    text-decoration: none;
}
</style>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

            <a href="{{ route('user.home') }}"><< {{ __('Back') }}</a>

            <!-- Profile -->
            <div class="card profile">
                <div class="card-header">{{ __('Employee Profile') }}</div>

                <div class="card-body">
                    @include('partials.formAlert')

                    <img class="pull-right" height="200" alt="{{ $company->name }}" src="{{ $company->logo ?? '/img/company-avatar.png' }}"/>
                    <div class="form-group">
                        <label>{{ __('Name') }}</label>
                        <div>{{ $company->name }}</div>
                    </div>
                    <div class="form-group">
                        <label>{{ __('Position') }}</label>
                        <div>{{ $company->position }}</div>
                    </div>
                    <div class="form-group">
                        <label>{{ __('Company Name') }}</label>
                        <div>{{ $company->company_name }}</div>
                    </div>
                    <div class="form-group">
                        <label>{{ __('Company Domain') }}</label>
                        <div>{{ $company->domain }}</div>
                    </div>
                    <div class="form-group">
                        <label>{{ __('Company Profile URL') }}</label>
                        <div><a href="{{ $company->url }}">{{ $company->url }}</a></div>
                    </div>
                    <div class="form-group">
                        <label>{{ __('Average Rating') }}</label>
                        <div class="rating rating-view" data-score="{{ $company->accepted_reviews->avg('score') }}"></div>
                    </div>                    
                    <div class="form-group">
                        <label>{{ __('Short Profile') }}</label>
                        <div>{!! $company->summary !!}</div>
                    </div>                    
                    <div class="form-group">
                        <label>{{ __('Problems') }}</label>
                        <div>{!! $company->problems !!}</div>
                    </div>
                </div>
            </div>

            <!-- Submit Review -->
            @include('partials.formReview', ['url' => route('user.review', ['account_id' => $company->account_id])])

            <!-- Reviews -->
            @include('partials.reviews')
        </div>
    </div>
</div>
@endsection
