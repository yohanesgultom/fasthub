@extends('layouts.app')

@section('title', __('Facilitator Home'))

@section('scripts')
<script src="{{ asset('libs/starrr/starrr.js') }}"></script>
<script src="{{ asset('js/rating.js') }}"></script>
@endsection

@section('styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="{{ asset('css/rating.css') }}">
<style>
.card {
    margin: 10px 0px;
}
.card label {
    color: gray;
}
.card-body img {
    margin-bottom: 10px;
}
.rating-view {
    font-size: 1.5em;
}
</style>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Facilitator Dashboard') }}</div>

                <div class="card-body">
                    @include('partials.formAlert')
                    
                    <div class="row">
                        <div class="col-lg-3 col-6 mb-1">
                            <a class="btn btn-block btn-success" href="{{ route('user.showMessagesForm') }}">
                                <i class="fa fa-comments" style="font-size:28pt"></i><br> 
                                <small class="{{ $new_message_count > 0 ? 'alert-label' : ''}}">
                                    {{ __('Messages') }} 
                                    {{ $new_message_count > 0 ? '('.$new_message_count.' '.__('new').')' : '' }}
                                </small>
                            </a>
                        </div>
                        <div class="col-lg-3 col-6 mb-1">
                            <a class="btn btn-block btn-info" href="{{ route('user.update') }}">
                                <i class="fa fa-user" style="font-size:28pt"></i><br> 
                                <small>{{ __('Edit profile') }}</small>
                            </a>
                        </div>
                        <div class="col-lg-3 col-6 mb-1">
                            <a class="btn btn-block btn-warning" href="{{ route('user.showChangePassword') }}">
                                <i class="fa fa-key" style="font-size:28pt"></i><br> 
                                <small>{{ __('Change Password') }}</small>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <h3>{{__('Employees')}}</h3>

            {{ $companies->links() }}

            @if (count($companies) <= 0)
            <div class="alert alert-info">{{ __('No data yet. Please check again later') }}</div>
            @endif

            @foreach ($companies as $c)
            <div class="card">
                <div class="card-header">
                    <i class="fa fa-building-o mr-1"></i>
                    {{ $c->name }} 
                    @if (!empty($c->position))
                    | <label><em>{{ $c->position }}</em></label>
                    @endif
                </div>
                <div class="card-body">                    
                    <img class="float-md-right" height="150" src="{{ $c->logo ?? 'img/company-avatar.png' }}"/>                    
                    <div class="form-group">
                        <label>{{ __('Company Name') }}</label>
                        <div>{{ $c->company_name }}</div>
                    </div>
                    @if (!empty($c->domain))
                    <div class="form-group">
                        <label>{{ __('Company Domain') }}</label>
                        <div>{{ $c->domain }}</div>
                    </div>
                    @endif
                    @if (!empty($c->url))
                    <div class="form-group">
                        <label>{{ __('Company Profile URL') }}</label>
                        <div><a href="{{ $c->url }}">{{ $c->url }}</a></div>
                    </div>
                    @endif
                    <div class="form-group">
                        <label>{{ __('Average Rating') }}</label>
                        <div class="rating rating-view" data-score="{{ $c->accepted_reviews->avg('score') }}"></div>
                    </div>
                    @if (!empty($c->problems))
                    <div class="form-group">
                        <label>{{ __('Problems') }}</label>
                        <div>{{ str_limit(strip_tags($c->problems), 200) }}</div>
                    </div>
                    @endif
                    <a href="{{ route('user.showCompany', ['id' => $c->id]) }}" class="btn btn-primary btn-details">
                    <i class="fa fa-search"></i>
                    {{ __('Details') }}
                    </a>
                    <a href="{{ route('user.chat', ['account_id' => $c->account_id ]) }}" class="btn btn-success btn-chat">
                        <i class="fa fa-comments-o"></i>
                        {{ __('Send Message') }}
                    </a>
                </div>
            </div>
            @endforeach

            {{ $companies->links() }}

        </div>
    </div>
</div>
@endsection
