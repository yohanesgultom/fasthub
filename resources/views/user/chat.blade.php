@extends('layouts.app')

@section('title', __('Send Message'))

@include('partials.chat', [
    'back_url' => route('user.home'),
    'reply_url' => route('user.reply', ['account_id' => $receiver->id]),
])