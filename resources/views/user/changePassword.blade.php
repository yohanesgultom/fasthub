@extends('layouts.app')

@section('title', __('Change Password'))

@section('content')
@include('partials.changePassword', ['back_url' => route('user.home'), 'submit_url' => route('user.changePassword')])
@endsection
