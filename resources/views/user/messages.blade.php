@extends('layouts.app')

@section('title', __('Messages'))

@include('partials.messages', [
    'user_type' => 'user',
])