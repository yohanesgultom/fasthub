@extends('layouts.app')

@section('title', __('My Facilitator Profile'))

@section('styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css">
<style>
.bootstrap-tagsinput {
    display: block;
}
</style>
@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.min.js"></script>
<script>
$(document).ready(function() {
    $('.summernote').summernote({
        height: 300,
        toolbar: [
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']]
        ]        
    });
    $('.tagsinput').tagsinput({
        tagClass: 'badge badge-info',
        trimValue: true,
        maxChars: 100,
        maxTags: 30,
    });
});
</script>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

            <a href="{{ route('user.home') }}"><< {{ __('Back') }}</a>
            
            <div class="card">
                <div class="card-header">{{ __('Facilitator Profile') }}</div>

                <div class="card-body">
                    @include('partials.formAlert')

                    <form method="POST" action="{{ route('user.update') }}" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group">
                            <label>{{ __('Name') }}</label>
                            <input type="text" name="name" class="form-control" placeholder="Enter your full name" value="{{ $user->name }}" required>
                        </div>
                        <div class="form-group">
                            <label>{{ __('Photo') }}</label>
                            <input type="file" name="photo" class="form-control">
                            <small class="form-text text-muted">{{ __('Extension: .png, .jpg, or .jpeg Max size: 500 KB') }}</small>
                            <small class="form-text text-muted">{{ __('By uploading photo, you agree to give us permission to display it to registered Employees') }}</small>
                            @if (!empty($user->photo))
                            <img width="200" src="{{ $user->photo }}"/>
                            @endif                                
                        </div>                        
                        <div class="form-group">
                            <label>{{ __('Email') }}</label>
                            <input type="email" name="email" class="form-control" placeholder="Enter your email" value="{{ $user->email }}" required>
                        </div>
                        <div class="form-group">
                            <label>{{ __('Title') }}</label>
                            <input type="text" name="title" class="form-control" placeholder="Enter your job or profession title" value="{{ $user->title }}" required>
                            <small class="form-text text-muted">{{ __('Examples') }}: Senior Data Scientist, Experienced Market Researcher, Manager of Sales Division</small>
                        </div>
                        <div class="form-group">
                            <label>{{ __('Competencies') }}</label>
                            <input type="text" name="skills" class="form-control tagsinput" placeholder="Enter your competencies or skills" value="{{ $user->skills->implode('name', ',') }}">
                            <small class="form-text text-muted">{{ __('Guide: Press ENTER after each item') }} {{ __('Examples') }} : internet marketing, human capital, growth hacking, agile software development</small>
                        </div>
                        <div class="form-group">
                            <label>{{ __('Education & Certification') }}</label>
                            <small class="form-text text-muted">{{ __('Examples') }} : 
                                <ol>
                                    <li>Bachelor of Business Administration SBM ITB (GPA 3.5/4.0)</li>
                                    <li>Master degree in Human Resources, Management Harvard University (GPA 3.9/4.0)</li>
                                    <li>Organization Specialist (Certified)</li>
                                    <li>Oracle Certified Java Programmer (OCJP 8)</li>
                                </ol>
                            </small>                            
                            <textarea class="summernote" name="education">{!! $user->education !!}</textarea>                            
                        </div>
                        <div class="form-group">
                            <label>{{ __('Work Experience') }}</label>
                            <small class="form-text text-muted">{{ __('Your working experience starting from the latest one') }}. {{ __('Examples') }} :<br><br> 
                                1. HRD Manager at XYZ LTD. (2012-Now)<br>
                                Job description:
                                <ul>
                                    <li>Supervises employee health insurance</li>
                                    <li>Manages employee recruitment process</li>
                                    <li>Supervises employee performance evaluations</li>
                                </ul>
                                2. HRD Performance Appraisal staff at ABC INC. (2010-2012)<br>
                                Job description:
                                <ul>
                                    <li>Performs KPI analysis</li>
                                    <li>Coordinates with department heads</li>
                                    <li>Executes employee performance evaluations</li>
                                </ul>
                            </small>   
                            <textarea class="summernote" name="experience">{!! $user->experience !!}</textarea>
                        </div>                        
                        <div class="form-group form-group-summary">
                            <label>{{ __('Summary') }}</label>
                            <small class="form-text text-muted">
                                {{ __('Show off your achievements and explain briefly why employer should hire you') }}. {{ __('Examples') }} :<br><br> 
                                <p>
                                    I managed to transform a family business into a professional world-class company (ABC INC.). In my current company (XYZ LTD.), I also have successfully implemented CBHRM and HRIS to improve human capital quality. XYZ INC. has been a market leader since I became the HRD Manager.
                                </p>
                            </small>
                            <textarea class="summernote" name="summary">{!! $user->summary !!}</textarea>                            
                        </div>
                        <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
