@extends('layouts.email')

@section('title', 'New Message')

@section('content')
<div class="container">
  <p>Hi {{ $user->name }},</p>  
  <br>

  @if (count($messages) > 1)
  <p>There are new messages for you:</p>  
  @else
  <p>There is a new message for you:</p>  
  @endif

  @foreach ($messages as $acc)
  <div>
    <label class="text-muted">{{ __('From') }}:</label>
    <span>{{ $acc['account_name'] }}</span>
  </div>
  <div>
    <div class="card">
      <div class="card-body">
        @foreach ($acc['messages'] as $m)
        <div>{!! $m->body !!}</div>
        @endforeach
        <small class="text-muted">Sent at {{ $acc['last_created_at']->toDayDateTimeString() }}</small>
      </div>
  </div>
  <br>
  @endforeach

  <br>
  <p><a href="{{ config('app.url') }}">Login</a> to check and reply {{ count($messages) > 1 ? 'them' : 'it' }}.</p>

  <br>
  <div class="text-muted">
    <div>--</div>
    <div>{{ config('app.name') }}</div>
  </div>
</div>
@endsection