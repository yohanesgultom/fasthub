@extends('layouts.email')

@section('title', 'Unread Message')

@section('content')
<div class="container">
  <p>Hi {{ $user->name }},</p>  
  <br>

  <p>
    Just a friendly reminder. You have <strong>{{ $count }} unread message{{ $count > 1 ? 's' : '' }}</strong>. 
    <a href="{{ config('app.url') }}">Login</a> to check and reply {{ $count > 1 ? 'them' : 'it' }}.
  </p>

  <br>
  <div class="text-muted">
    <div>--</div>
    <div>{{ config('app.name') }}</div>
  </div>
</div>
@endsection