@extends('layouts.email')

@section('title', 'Contact Us Email')

@section('content')
<div class="container">
  <p>There is new message sent through contact us form:</p>  
  <div>
    <label class="text-muted">{{ __('Email') }}:</label>
    <span>{{ $email }}</span>
  </div>
  <div>
    <label class="text-muted">{{ __('Sent at') }}:</label>
    <span>{{ strftime('%A, %d %B %Y %R %p', $sent_at->getTimeStamp()) }}</span>
  </div>
  @if (!empty($user))
  <div>
    <label class="text-muted">{{ __('Account Type') }}:</label>
    <span>{{ get_class($user) }}</span>
  </div>
  <div>
    <label class="text-muted">{{ __('Account Email') }}:</label>    
    <span>{{ $user->email }}</span>
  </div>  
  <div>
    <label class="text-muted">{{ __('Account Name') }}:</label>    
    <span>{{ $user->name }}</span>
  </div>  
  @endif    
  <div>
    <label class="text-muted">{{ __('Message') }}:</label>
    <div class="card">
      <div class="card-body">
        {{ $msg }}
      </div>
  </div>
</div>
@endsection