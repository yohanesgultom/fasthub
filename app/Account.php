<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cmgmyr\Messenger\Traits\Messagable;
use Carbon\Carbon;

class Account extends Model
{
    use Messagable;

    protected $fillable = [
        'name',
        'avatar',
    ];

    protected $dates = [
        'last_email',
    ];

    public function user()
    {
        return $this->hasOne('App\User');
    }

    public function company()
    {
        return $this->hasOne('App\Company');
    }

    public function participants()
    {
        return $this->hasMany('\Cmgmyr\Messenger\Models\Participant', 'user_id');
    }

    public function updateLastEmail()
    {
        $this->last_email = Carbon::now();
        $this->save();
    }
}
