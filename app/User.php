<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'name',
        'email',
        'password',
        'title',
        'summary',        
        'experience',        
        'education',
        'photo',
        'account_id',
        'verified',
        'email_token',
    ];

    protected $hidden = [
        'password',
        'remember_token',
        'email_token',
        'verified',
    ];

    protected $casts = [
        'verified' => 'boolean',
    ];

    protected static function boot()
    {
        static::creating(function ($model) {
            $account = Account::create([
                'name' => $model->name,
                'avatar' => $model->photo,
            ]);
            $model->account_id = $account->id;
        });

        static::updating(function ($model) {
            Account::where('id', $model->account_id)->update([
                'name' => $model->name,
                'avatar' => $model->photo,
            ]);
        });        
    }

    public function skills()
    {
        return $this->belongsToMany('App\Skill');
    }

    public function account()
    {
        return $this->belongsTo('App\Account');
    }

    public function accepted_reviews()
    {
        return $this->hasMany('App\ReviewScore', 'reviewee_id', 'account_id');
    }

    public function given_reviews()
    {
        return $this->hasMany('App\ReviewScore', 'reviewer_id', 'account_id');
    }
}
