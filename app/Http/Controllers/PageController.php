<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\EmailContactUs;

class PageController extends Controller
{
    public function welcome()
    {
        return view('welcome');
    }
    
    public function termsConditions()
    {
        return view('terms-conditions');
    }

    public function contactUs(Request $request)
    {        
        $request->validate([
            'email' => 'email|required',
            'message' => 'required',
            'g-recaptcha-response' => 'required|recaptcha',
        ]);
        $input = $request->only('email', 'message');
        $email = new EmailContactUs($input['email'], $input['message']);
        Mail::to(config('app.support_emails'))->queue($email);
        $request->session()->flash('status', __('Thank you for your message!'));
        return redirect(route('welcome').'#contact-us');
    }
}
