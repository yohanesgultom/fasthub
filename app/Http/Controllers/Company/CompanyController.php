<?php

namespace App\Http\Controllers\Company;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Hash;
use Sanitizer;
use App\Account;
use App\User;
use App\ReviewScore;
use Cmgmyr\Messenger\Models\Thread;
use App\Service\MessageService;

class CompanyController extends Controller
{
    public function update(Request $request)
    {
        $request->validate([
            'logo' => 'mimes:jpeg,jpg,png|max:500',
        ]);        
        $existing = Auth::user();
        $input = $request->except('verified', 'email_token', 'remember_token');
        $sanitizer = new Sanitizer();
        $input['summary'] = $sanitizer->sanitize($input['summary']);
        $existing->fill($input);
        if ($request->has('logo')) {
            $filename = str_slug($existing->name).'.'.$request->file('logo')->getExtension();
            $existing->logo = '/storage/'.$request->file('logo')->storeAs('logo', $filename, 'public');    
        }
        $existing->save();
        return redirect()
            ->route('company.update')
            ->with('status', __('Profile updated'));
    }
    
    public function reply(Request $request, $account_id)
    {
        $request->validate([
            'message' => 'required',
        ]);
        $sender_id = Auth::user()->account_id;
        $receiver_id = $account_id;
        $thread = MessageService::firstThreadOrCreate($sender_id, $receiver_id);
        $message = $request->get('message');
        MessageService::addMessage($thread, Auth::user()->account_id, $message);
        return redirect()->route('company.chat', ['account_id' => $account_id]);
    }
    
    public function review(Request $request, $account_id)
    {        
        $request->validate([
            'score' => 'required|digits_between:1,5',
            'body' => 'required|min:2',
        ]);
        
        try {
            $user = User::where('account_id', $account_id)->first();
            if (empty($user)) {
                throw new \Exception('No reviewee with account_id '.$account_id);
            }
            $input = $request->only(['score', 'body']);
            $review = new ReviewScore;
            $review->fill($input);
            $review->reviewee_id = $account_id;
            $review->reviewer_id = Auth::user()->account_id;
            $review->save();        
            $request->session()->flash('status', __('Review successfully submitted'));
        } catch (\Exception $e) {
            $error = $e->getMessage();
            if (str_contains($error, 'Duplicate entry')) {
                $error = 'Duplicate entry';
            }
            $request->session()->flash('error', $error);
        }                
        return redirect()->route('company.showUser', ['id' => $user->id]);
    }

    public function changePassword(Request $request)
    {        
        $request->validate([
            'password' => 'required',
            'password_new' => 'required|min:6|max:20|different:password|confirmed',
        ]);                
        $existing = Auth::user();
        $input = $request->only('password', 'password_new');
        if (Hash::check($input['password'], $existing->password)) {
            $existing->password = Hash::make($input['password_new']);
            $existing->save();
            $request->session()->flash('status', __('Password changed'));
            return redirect()->route('company.home');
        } else {
            $request->session()->flash('error', __('Invalid password'));
            return back();
        }
    }    
}
