<?php

namespace App\Http\Controllers\Company\Auth;

use App\Company;
use App\Mail\EmailVerification;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Auth;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class RegisterController extends Controller
{
    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/company/login';

    public function __construct()
    {
        $this->middleware('guest:company');
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'company_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:companies',
            'password' => 'required|string|min:6|confirmed',
            'g-recaptcha-response' => 'required|recaptcha',
        ]);
    }

    protected function create(array $data)
    {
        return Company::create([
            'name' => $data['name'],
            'company_name' => $data['company_name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']), 
            'verified' => false,
            'email_token' => bin2hex(random_bytes(30)),           
        ]);
    }

    public function showRegistrationForm()
    {
        return view('company.auth.register');
    }

    protected function guard()
    {
        return Auth::guard('company');
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();
        event(new Registered($user = $this->create($request->except('verified', 'email_token'))));
        $email = new EmailVerification($user);
        Mail::to($user->email)->queue($email);
        $request->session()->flash('status', __('Registration success. Please check you email to proceed with verification'));
        return redirect(route('company.login'));
    }

    /**
     * Handle a registration request for the application.
     *
     * @param $token
     * @return \Illuminate\Http\Response
     */
    public function verifyAccount(Request $request, $token)
    {
        $user = Company::where('email_token', $token)->first();
        if (empty($user)) {
            $request->session()->flash('error', __('Invalid token'));
            return redirect(route('company.register'));
        }        
        $user->verified = true;
        $user->email_token = null;
        if($user->save()){
            $this->guard()->login($user);
            return redirect(route('company.home'));
        }
    }     
}
