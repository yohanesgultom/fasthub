<?php

namespace App\Http\Controllers\Company\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/company';

    public function __construct()
    {
        $this->middleware(['guest:web', 'guest:company'])->except('logout');
    }

    public function showLoginForm()
    {
        return view('company.auth.login');
    }

    protected function guard()
    {
        return Auth::guard('company');
    }

    protected function validateLogin(Request $request)
    {
        $this->validate($request, [
            $this->username() => 'required|string',
            'password' => 'required|string',
            'g-recaptcha-response' => 'required|recaptcha',
        ]);
    }    

    /**
     * Attempt to log the user into the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    protected function attemptLogin(Request $request)
    {
        $result = $this->guard()->attempt(
            $this->credentials($request), $request->filled('remember')
        );        
        if ($result && !$this->guard()->user()->verified) {
            $request->session()->flash('error', 'Please verify your email address');
            $this->guard()->logout();
            $result = false;
        }
        return $result;
    }    
}
