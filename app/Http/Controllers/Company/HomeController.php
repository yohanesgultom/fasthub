<?php

namespace App\Http\Controllers\Company;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Skill;
use App\ReviewScore;
use App\Account;
use App\Service\MessageService;
use Auth;

class HomeController extends Controller
{
    public function index(Request $request)
    {                   
        $query = User::with(['accepted_reviews', 'skills'])->where('verified', true);
        $skills = $request->get('skills', '');
        if (!empty($skills)) {
            $search_skills = explode(',', $skills);
            if (!empty($search_skills)) {
                $query_skill = Skill::select('id');
                foreach ($search_skills as $s) {
                    $query_skill = $query_skill->orWhere('name', 'like', '%'.$s.'%');
                }
                $skill_ids = $query_skill->pluck('id');
                $query = $query->whereHas('skills', function ($q) use ($skill_ids) {
                    $q->whereIn('skills.id', $skill_ids);
                });
            }            
        }
        $facilitators = $query->orderBy('photo', 'desc')->simplePaginate(5);
        $new_message_count = MessageService::countUnreadMessages(Auth::user()->account->id);
        return view('company.home', compact('facilitators', 'skills', 'new_message_count'));
    }

    public function showUserForm(Request $request, $id)
    {        
        $user = User::with(['skills', 'accepted_reviews'])->find($id);
        if (empty($user)) {
            abort(404);
        }

        $reviews = ReviewScore::with('reviewer')
            ->where('reviewee_id', $user->account_id)
            ->paginate(5);

        return view('company.showUser', compact('user', 'reviews'));        
    }

    public function showUpdateForm()
    {
        $company = Auth::user();        
        return view('company.profile', [
            'company' => $company,
        ]);
    } 

    public function showChatForm($receiver_id)
    {        
        $sender_id = Auth::user()->account_id;
        $thread = MessageService::firstThreadOrCreate($sender_id, $receiver_id);
        $receiver = Account::find($receiver_id);
        $thread->markAsRead($sender_id);
        return view('company.chat', compact('thread', 'receiver'));
    }

    public function showChangePasswordForm()
    {
        return view('company.changePassword');
    }   

    public function showMessagesForm(Request $request)
    {
        $messages = MessageService::getLastMessages(Auth::user()->account->id);
        return view('company.messages', compact('messages'));
    }
}
