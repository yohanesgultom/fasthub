<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Company;
use App\ReviewScore;
use App\Account;
use App\Service\MessageService;
use Auth;

class HomeController extends Controller
{
    public function index()
    {
        $companies = Company::with('accepted_reviews')->simplePaginate(5);
        $new_message_count = MessageService::countUnreadMessages(Auth::user()->account->id);
        return view('user.home', compact('companies', 'new_message_count'));
    }

    public function showCompanyForm(Request $request, $id)
    {        
        $company = Company::with(['accepted_reviews'])->find($id);
        if (empty($company)) {
            abort(404);
        }

        $reviews = ReviewScore::with('reviewer')
            ->where('reviewee_id', $company->account_id)
            ->paginate(5);

        return view('user.showCompany', compact('company', 'reviews'));
    }

    public function showUpdateForm()
    {
        $user = Auth::user()->load('skills');        
        return view('user.profile', [
            'user' => $user,
        ]);
    }

    public function showChatForm($receiver_id)
    {        
        $sender_id = Auth::user()->account_id;
        $thread = MessageService::firstThreadOrCreate($sender_id, $receiver_id);
        $receiver = Account::find($receiver_id);
        $thread->markAsRead($sender_id);
        return view('user.chat', compact('thread', 'receiver'));
    }    

    public function showChangePasswordForm()
    {
        return view('user.changePassword');
    }

    public function showMessagesForm()
    {
        $messages = MessageService::getLastMessages(Auth::user()->account->id);
        return view('user.messages', compact('messages'));
    }
}
