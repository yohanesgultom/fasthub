<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Sanitizer;
use App\Account;
use App\Company;
use App\Skill;
use App\ReviewScore;
use Cmgmyr\Messenger\Models\Thread;
use App\Service\MessageService;
use Hash;

class UserController extends Controller
{

    public function update(Request $request)
    {        
        $request->validate([
            'photo' => 'mimes:jpeg,jpg,png|max:500',
        ]);                
        $existing = Auth::user()->load('skills');
        $input = $request->except('verified', 'email_token', 'remember_token');
        $sanitizer = new Sanitizer();
        $input['summary'] = $sanitizer->sanitize($input['summary']);
        $existing->fill($input);
        if ($request->has('photo')) {
            $filename = $existing->id.'_'.$request->file('photo')->getClientOriginalName();
            $existing->photo = '/storage/'.$request->file('photo')->storeAs('userphoto', $filename, 'public');    
        }
        $existing->save();
        if (array_key_exists('skills', $input)) {
            $skill_ids = [];
            $skill_names = explode(',', strtolower($input['skills']));
            if (count($skill_names) > 0) {
                $skills = Skill::whereIn('name', $skill_names)->select('id', 'name')->get();
                $new_skills = [];
                foreach ($skill_names as $name) {
                    $skill = $skills->where('name', $name)->first();
                    if (empty($skill)) {
                        $skill = Skill::create(['name' => $name]);
                    }
                    array_push($skill_ids, $skill->id);
                }
            }
            $existing->skills()->sync($skill_ids);    
        }
        return redirect()
            ->route('user.update')
            ->with('status', __('Profile updated'));
    }
    
    public function reply(Request $request, $account_id)
    {
        $request->validate([
            'message' => 'required',
        ]);
        $sender_id = Auth::user()->account_id;
        $receiver_id = $account_id;
        $thread = MessageService::firstThreadOrCreate($sender_id, $receiver_id);
        $message = $request->get('message');
        MessageService::addMessage($thread, Auth::user()->account_id, $message);
        return redirect()->route('user.chat', ['account_id' => $account_id]);
    }

    public function review(Request $request, $account_id)
    {        
        $request->validate([
            'score' => 'required|digits_between:1,5',
            'body' => 'required|min:2',
        ]);
        
        try {
            $company = Company::where('account_id', $account_id)->first();
            if (empty($company)) {
                throw new \Exception('No reviewee with account_id '.$account_id);
            }
            $input = $request->only(['score', 'body']);
            $review = new ReviewScore;
            $review->fill($input);
            $review->reviewee_id = $account_id;
            $review->reviewer_id = Auth::user()->account_id;
            $review->save();        
            $request->session()->flash('status', __('Review successfully submitted'));
        } catch (\Exception $e) {
            $error = $e->getMessage();
            if (str_contains($error, 'Duplicate entry')) {
                $error = 'Duplicate entry';
            }
            $request->session()->flash('error', $error);
        }                
        return redirect()->route('user.showCompany', ['id' => $company->id]);
    }

    public function changePassword(Request $request)
    {        
        $request->validate([
            'password' => 'required',
            'password_new' => 'required|min:6|max:20|different:password|confirmed',
        ]);                
        $existing = Auth::user();
        $input = $request->only('password', 'password_new');
        if (Hash::check($input['password'], $existing->password)) {
            $existing->password = Hash::make($input['password_new']);
            $existing->save();
            $request->session()->flash('status', __('Password changed'));
            return redirect()->route('user.home');
        } else {
            $request->session()->flash('error', __('Invalid password'));
            return back();
        }
    }

}
