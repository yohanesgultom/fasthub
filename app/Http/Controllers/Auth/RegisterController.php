<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Mail\EmailVerification;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;


class RegisterController extends Controller
{
    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    public function __construct()
    {
        $this->middleware('guest');
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'g-recaptcha-response' => 'required|recaptcha',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'title' => $data['title'],
            'password' => Hash::make($data['password']),
            'verified' => false,
            'email_token' => bin2hex(random_bytes(30)),
        ]);
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();
        event(new Registered($user = $this->create($request->except('verified', 'email_token'))));
        $email = new EmailVerification($user);
        Mail::to($user->email)->queue($email);
        $request->session()->flash('status', __('Registration success. Please check you email to proceed with verification'));
        return redirect(route('login'));
    }

    /**
     * Handle a registration request for the application.
     *
     * @param $token
     * @return \Illuminate\Http\Response
     */
    public function verifyAccount(Request $request, $token)
    {
        $user = User::where('email_token', $token)->first();
        if (empty($user)) {
            $request->session()->flash('error', __('Invalid token'));
            return redirect(route('register'));
        }        
        $user->verified = true;
        $user->email_token = null;
        if($user->save()){
            $this->guard()->login($user);
            return redirect(route('user.home'));
        }
    }    
}
