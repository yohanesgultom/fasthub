<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        \Cmgmyr\Messenger\Models\Models::setUserModel(\App\Account::class);
        \Cmgmyr\Messenger\Models\Models::setTables([
            'users' => 'accounts',
        ]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
