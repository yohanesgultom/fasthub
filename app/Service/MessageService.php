<?php

namespace App\Service;

use Carbon\Carbon;
use Cmgmyr\Messenger\Models\Message;
use Cmgmyr\Messenger\Models\Participant;
use Cmgmyr\Messenger\Models\Thread;
use DB;
use App\Account;

class MessageService
{
    /**
     * Find or create a thread having given two users as participants
     */
    public static function firstThreadOrCreate($sender_id, $receiver_id)
    {   
        $thread = Thread::with(['messages.user', 'participants'])
            ->between([$sender_id, $receiver_id])->first();
        if (empty($thread)) {
            $thread = MessageService::createThread('A chat', $sender_id, $receiver_id);
        }
        return $thread;
    }

    /**
     * Create a new thread containing two users as participants
     */
    public static function createThread($subject, $sender_id, $recipient_id)
    {
        $thread = Thread::create([
            'subject' => $subject,
        ]);
        $thread->addParticipant($sender_id);
        $thread->addParticipant($recipient_id);
        return $thread;
    }

    /**
     * Add message from a user to existing thread
     * If the user is not yet a participant of the thread,
     * add him/her as participant 
     */
    public static function addMessage($thread, $sender_id, $message)
    {
        Message::create([
            'thread_id' => $thread->id,
            'user_id' => $sender_id,
            'body' => $message,
        ]);

        // Add replier as a participant
        $participant = Participant::firstOrCreate([
            'thread_id' => $thread->id,
            'user_id' => $sender_id,
        ]);
        $participant->last_read = new Carbon;
        $participant->save();
    }

    /**
     * Count threads with unread messages
     */
    public static function countUnreadMessages($receiver_id)
    {
        return count(Message::unreadForUser($receiver_id)
            ->groupBy('messages.thread_id')
            ->selectRaw('messages.thread_id')
            ->get());
    }

    /**
     * Get last message per threads
     */
    public static function getLastMessages($receiver_id)
    {
        return Message::whereIn('messages.id', function ($query) use ($receiver_id) {
                $query->selectRaw('max(m.id)')
                    ->from('messages as m')
                    ->join('participants as p', 'p.thread_id', 'm.thread_id')
                    ->where('p.user_id', $receiver_id)
                    ->groupBy('m.thread_id');
            })
            ->join('participants', function ($join) use ($receiver_id) {
                $join->on('participants.thread_id', 'messages.thread_id')
                    ->where('participants.user_id', '!=', $receiver_id);
            })
            ->join('accounts', 'accounts.id', 'messages.user_id')
            ->join('accounts as receiver', 'receiver.id', 'participants.user_id')
            ->select(
                'messages.*',
                'accounts.name',
                'accounts.avatar',
                'receiver.id as receiver_id',
                'receiver.name as receiver_name',
                'receiver.avatar as receiver_avatar'
            )
            ->orderBy('messages.created_at', 'desc')->get();
    }

    public static function getUnreadMessagesForUser($receiver_id, $from_datetime)
    {
        $query = Message::whereHas('participants', function ($q1) use ($receiver_id) {
                $q1->where('participants.user_id', $receiver_id)->where(function ($q2) {
                        $q2->where('participants.last_read', '<', 'messages.created_at')
                            ->orWhereNull('participants.last_read');
                    });
            })
            ->join('accounts', 'accounts.id', 'messages.user_id')
            ->where('messages.created_at', '>=', $from_datetime)
            ->where('messages.user_id', '!=', $receiver_id)
            ->select(
                'messages.*',
                'accounts.id as account_id',
                'accounts.name',
                'accounts.avatar'
            );
        return $query->get();
    }

    /**
     * Get accounts who have at least one message created after $from_datetime
     */
    public static function getAccountsWithMessage($from_datetime)
    {
        $query = Account::with(['user', 'company'])
            ->join('participants', 'participants.user_id', 'accounts.id')
            ->join('messages', 'messages.thread_id', 'participants.thread_id')
            ->whereColumn('messages.user_id', '!=', 'participants.user_id')
            ->where('messages.created_at', '>=', $from_datetime)
            ->select('accounts.*')
            ->groupBy('accounts.id');
        return $query->get();
    }

    /**
     * Get accounts that has last read before $before_datetime
     */
    public static function getAccountsWithLastReadBefore($datetime)
    {
        $query = Account::with(['user', 'company'])
            ->join('participants', 'participants.user_id', 'accounts.id')
            ->join('messages', 'messages.thread_id', 'participants.thread_id')
            ->whereColumn('messages.user_id', '!=', 'participants.user_id')
            ->where(function ($q1) use ($datetime) {
                $q1->orWhere('participants.last_read', '<=', $datetime)
                    ->orWhereNull('participants.last_read');
            })
            ->select('accounts.*')
            ->groupBy('accounts.id');
        return $query->get();
    }
}