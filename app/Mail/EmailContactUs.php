<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Auth;

class EmailContactUs extends Mailable
{
    use Queueable, SerializesModels;    

    public $tries = 5;

    public $email;
    public $message;
    public $sent_at;
    public $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email, $message)
    {
        $this->email = $email;
        $this->message = $message;
        $this->sent_at = new \DateTime();
        if (Auth::check()) {                        
            $this->user = Auth::user();
        } else if (Auth::guard('company')->check()) {
            $this->user = Auth::guard('company')->user();
        }
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this            
            ->subject('['.config('app.name').'] '.__('New contact us message'))            
            ->view('email.contactUs')
            ->with([
                'email' => $this->email,
                'msg' => $this->message,
                'sent_at' => $this->sent_at,
                'user' => $this->user,
            ]);
    }
}
