<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EmailVerification extends Mailable
{
    use Queueable, SerializesModels;

    public $tries = 5;

    public $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $template = null;
        if ($this->user instanceof \App\User) {
            $template = 'user.email.verifyAccount';            
        } else if ($this->user instanceof \App\Company) {
            $template = 'company.email.verifyAccount';
        } else {
            throw new \Exception('Invalid user type: '.get_class($this->user));
        }
        return $this
            ->subject('['.config('app.name').'] '.__('Please verify your email address')) 
            ->view($template)
            ->with([
            'email_token' => $this->user->email_token,
        ]);    
    }
}
