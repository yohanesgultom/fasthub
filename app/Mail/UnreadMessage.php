<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UnreadMessage extends Mailable
{
    use Queueable, SerializesModels;

    public $tries = 5;
    
    public $user;
    public $count;

    public function __construct($user, $count)
    {
        $this->user = $user;
        $this->count = $count;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject('['.config('app.name').'] '.__('You have unread message'))
            ->view('email.unreadMessage')
            ->with([
                'user' => $this->user,
                'count' => $this->count,
            ]);
    }
}
