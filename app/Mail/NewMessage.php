<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewMessage extends Mailable
{
    use Queueable, SerializesModels;

    public $tries = 5;
    
    public $user;
    public $messages;

    public function __construct($user, $messages)
    {
        $this->user = $user;
        // group messages by account details
        $this->messages = [];
        foreach ($messages as $m) {
            if (!array_key_exists($m->account_id, $this->messages)) {
                $this->messages[$m->account_id] = [
                    'account_id' => $m->account_id,
                    'account_name' => $m->name,
                    'account_avatar' => $m->avatar,
                    'messages' => [],
                ];
            }
            $this->messages[$m->account_id]['messages'][] = $m;
            $this->messages[$m->account_id]['last_created_at'] = $m->created_at;
        }
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject('['.config('app.name').'] '.__('You have new message'))
            ->view('email.newMessage')
            ->with([
                'user' => $this->user,
                'messages' => $this->messages,            
            ]);
    }
}
