<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Service\MessageService;
use App\Mail\UnreadMessage;
use App\Account;
use Carbon\Carbon;
use Mail;

class UnreadMessageJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 1;

    public $datetime;

    public function __construct($datetime = null)
    {
        $this->datetime = $datetime ?? Carbon::now()->subDay();
    }

    public function handle()
    {
        // get accounts with last_read before $this->datetime
        $accounts = MessageService::getAccountsWithLastReadBefore($this->datetime);
        // check unread new message for each account
        foreach ($accounts as $acc) {            
            // handle overlapping messages using last_email            
            if (empty($acc->last_email) || $acc->last_email->diffInDays(Carbon::now(), true) >= 3) {                
                if (!empty($acc->last_email)) \Log::debug('Diff $acc->last_email', [$acc->last_email->diffInDays(Carbon::now(), true)]);
                $count = MessageService::countUnreadMessages($acc->id);
                if ($count > 0) {                
                    // send notification if there is any new message
                    $user = $acc->user ?? $acc->company;
                    Mail::to($user->email)->queue(new UnreadMessage($user, $count));
                    // update last email
                    $acc->updateLastEmail();
                }   
            }
        } 
    }
}
