<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Service\MessageService;
use App\Mail\NewMessage;
use App\Account;
use Carbon\Carbon;
use Mail;

class NewMessageJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 1;

    public $from_date;

    public function __construct($from_date = null)
    {
        // by default looks up new message since last 2 hours
        // assuming job runs once an hour
        // overlapping messages are taken care by accounts.last_email field
        $this->from_date = $from_date ?? Carbon::now()->subHours(2);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // get accounts with new message (read/unread)
        $accounts = MessageService::getAccountsWithMessage($this->from_date);
        // check unread new message for each account
        foreach ($accounts as $acc) {            
            // handle overlapping messages using last_email
            $from_date = $acc->last_email ?? $this->from_date;
            $messages = MessageService::getUnreadMessagesForUser($acc->id, $from_date);
            if (count($messages) > 0) {                
                // send notification if there is any new message
                $user = $acc->user ?? $acc->company;
                Mail::to($user->email)->queue(new NewMessage($user, $messages));
                // update last email
                $acc->updateLastEmail();
            }
        } 

    }
}
