<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReviewScore extends Model
{
    protected $fillable = [
        'score',
        'body',
        'reviewee_id',
        'reviewer_id',
        'public',
    ];

    protected $casts = [
        'public' => 'boolean',
    ];

    public function reviewer()
    {
        return $this->belongsTo('App\Account', 'reviewer_id');
    }
    
    public function reviewee()
    {
        return $this->belongsTo('App\Account', 'reviewee_id');
    }
}
