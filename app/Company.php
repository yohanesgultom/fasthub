<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Company extends Authenticatable
{
    use Notifiable;

    protected $guard = 'companies';

    protected $fillable = [
        'name',
        'company_name',
        'position',
        'domain',
        'email',
        'url',
        'password',
        'summary',
        'problems',
        'logo',
        'account_id',
        'email_token',
        'verified',
    ];

    protected $hidden = [
        'password',
        'remember_token',
        'email_token',
        'verified',
    ];
        
    protected $casts = [
        'verified' => 'boolean',
    ];

    protected static function boot()
    {
        static::creating(function ($model) {
            $account = Account::create([
                'name' => $model->name,
                'avatar' => $model->logo,
            ]);
            $model->account_id = $account->id;
        });

        static::updating(function ($model) {
            Account::where('id', $model->account_id)->update([
                'name' => $model->name,
                'avatar' => $model->logo,
            ]);
        });        
    }
    
    public function account()
    {
        return $this->belongsTo('App\Account');
    }

    public function accepted_reviews()
    {
        return $this->hasMany('App\ReviewScore', 'reviewee_id', 'account_id');
    }

    public function given_reviews()
    {
        return $this->hasMany('App\ReviewScore', 'reviewer_id', 'account_id');
    }
}
