/* Display star rating using font-awesome css */

$(document).ready(function() {
    // rating
    $('.rating-input')
        .starrr({ max: 5 })
        .on('starrr:change', function(e, value){
            $(this).next('input[name=score]').val(value);
        });
    $('.rating-view').each(function() {
        let score = Math.round($(this).data('score') * 2) / 2;
        $(this).starrr({ max: 5, readOnly: true, rating: score });
        let label = $('<small>').addClass('ml-1').text(score);
        $(this).append(label);
    })
        
});
        
