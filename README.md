# Facilitator Hub

## Setup and Test

To setup and run tests, follow these steps:
1. Install dependencies `composer install`
1. Migrate database `php artisan migrate:fresh` (add `--seed` option for seed data)
1. Run functional tests `./serve-dusk`